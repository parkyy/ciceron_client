package starlord.ciceron.mvp.common;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by JaehongPark on 14. 11. 23..
 */
public class Fonts {
    private Context context;
    private static Fonts instance;
    private Typeface font_gotham;
    private Typeface font_zeronero;
    private Typeface font_appleSDGothic_Regular;
    private Typeface font_appleSDGothic_Bold;


    public Fonts(Context context) {
        font_gotham = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/gotham_medium.otf");
        font_zeronero = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/zeronero.ttf");
        font_appleSDGothic_Regular = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/AppleSDGothicNeo-Regular.otf");
        font_appleSDGothic_Bold = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/AppleSDGothicNeo-Bold.otf");
    }

    public static Fonts create(Context context) {
        if (instance == null) {
            instance = new Fonts(context);
        }
        return instance;
    }

    public Typeface gotham() {
        return font_gotham;
    }

    public Typeface zeronero() {
        return font_zeronero;
    }

    public Typeface appleGothic() { return font_appleSDGothic_Regular; }

    public Typeface appleGothicBold() { return font_appleSDGothic_Bold; }
}
