package starlord.ciceron.mvp.common;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.io.File;

import starlord.ciceron.mvp.BuildConfig;
import starlord.ciceron.mvp.image.ImageCache;
import starlord.ciceron.mvp.image.ImageLoader;
import starlord.ciceron.mvp.network.VolleyManager;

/**
 * Created by JaehongPark on 15. 2. 8..
 */
public class CiceronApp extends Application {
    private static CiceronApp appInstance;
    private Context appContext;
    private ImageLoader imageLoader;
    private String DEBUG_TAG = "[CiceronApp]";


    @Override
    public void onCreate() {
        super.onCreate();

        if(BuildConfig.DEBUG) {
            Log.d(DEBUG_TAG, "onCreate");
        }

        appInstance = this;
        appContext = getApplicationContext();

        ImageCache.ImageCacheParams imageCacheParams
                = new ImageCache.ImageCacheParams(appContext, "image");
        imageCacheParams.setMemCacheSizePercent(0.25f);
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(appContext.getResources(), imageCacheParams);
    }



    @Override
    public void onTerminate() {
        super.onTerminate();

        if(BuildConfig.DEBUG) {
            Log.d(DEBUG_TAG, "onTerminate");
        }

        imageLoader.close();
        imageLoader = null;

        VolleyManager.getInstance().cancelPendingRequests();
    }


    public static CiceronApp getAppInstance() {
        return appInstance;
    }

    public Context getAppContext() {
        return appContext;
    }

}
