package starlord.ciceron.mvp.common;

import java.util.ArrayList;
import java.util.HashMap;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 5..
 */
public class Format {
    private static ArrayList<String> format_list = new ArrayList<String>();
    private static HashMap<Integer, Integer> format_drawable = new HashMap<Integer, Integer>();

    static {
        format_list.add("전자우편");
        format_list.add("편지");
        format_list.add("에세이");
        format_list.add("광고");
        format_list.add("안부");
        format_list.add("출판물");
        format_list.add("대본");
        format_list.add("리서치");
        format_list.add("메뉴");
        format_list.add("지원서");
        format_list.add("설명");
        format_list.add("SNS");
        format_list.add("초대장");
        format_list.add("레포트");
        format_list.add("기타");

        format_drawable.put(0, R.drawable.ic_format_email);
        format_drawable.put(1, R.drawable.ic_format_letter);
        format_drawable.put(2, R.drawable.ic_format_essay);
        format_drawable.put(3, R.drawable.ic_format_commercial);
        format_drawable.put(4, R.drawable.ic_format_greeting);
        format_drawable.put(5, R.drawable.ic_format_publishing);
        format_drawable.put(6, R.drawable.ic_format_script);
        format_drawable.put(7, R.drawable.ic_format_research);
        format_drawable.put(8, R.drawable.ic_format_menu);
        format_drawable.put(9, R.drawable.ic_format_application);
        format_drawable.put(10, R.drawable.ic_format_instruction);
        format_drawable.put(11, R.drawable.ic_format_sns);
        format_drawable.put(12, R.drawable.ic_format_invitation);
        format_drawable.put(13, R.drawable.ic_format_report);
        format_drawable.put(14, R.drawable.ic_format_etc);
    }

    public static ArrayList<String> getList() {
        return format_list;
    }

    public static Integer getDrawable(int format) {
        if(format_drawable.containsKey(format))
            return format_drawable.get(format);
        return null;
    }

    public static String getTitle(int format) {
        return format_list.get(format);
    }
}
