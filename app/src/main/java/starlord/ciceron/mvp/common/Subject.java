package starlord.ciceron.mvp.common;

import java.util.ArrayList;
import java.util.HashMap;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 5..
 */
public class Subject {
    private static ArrayList<String> subject_list = new ArrayList<String>();
    private static HashMap<Integer, Integer> subject_drawable = new HashMap<Integer, Integer>();

    static {
        subject_list.add(0, "예술");
        subject_list.add(1, "스포츠");
        subject_list.add(2, "과학");
        subject_list.add(3, "건강");
        subject_list.add(4, "경제");
        subject_list.add(5, "음악");
        subject_list.add(6, "문학");
        subject_list.add(7, "정치");
        subject_list.add(8, "비즈니스");
        subject_list.add(9, "종교");
        subject_list.add(10, "쇼핑");
        subject_list.add(11, "패션");

        subject_drawable.put(0,  R.drawable.ic_subject_art);
        subject_drawable.put(1,  R.drawable.ic_subject_sport);
        subject_drawable.put(2,  R.drawable.ic_subject_science);
        subject_drawable.put(3,  R.drawable.ic_subject_health);
        subject_drawable.put(4,  R.drawable.ic_subject_economy);
        subject_drawable.put(5,  R.drawable.ic_subject_music);
        subject_drawable.put(6,  R.drawable.ic_subject_literature);
        subject_drawable.put(7,  R.drawable.ic_subject_politics);
        subject_drawable.put(8,  R.drawable.ic_subject_business);
        subject_drawable.put(9,  R.drawable.ic_subject_religion);
        subject_drawable.put(10, R.drawable.ic_subject_shopping);
        subject_drawable.put(11, R.drawable.ic_subject_fashion);
    }

    public static ArrayList<String> getList() {
        return subject_list;
    }

    public static Integer getDrawable(Integer subject) {
        if(subject_drawable.containsKey(subject)) {
            return subject_drawable.get(subject);
        }
        return null;
    }

    public static String getTitle(int subject) {
        return subject_list.get(subject);
    }
}
