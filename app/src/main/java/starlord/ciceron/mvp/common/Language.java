package starlord.ciceron.mvp.common;

import java.util.ArrayList;
import java.util.HashMap;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 5..
 */
public class Language {
    private static ArrayList<String> lang_list = new ArrayList<String>();
    private static HashMap<Integer, Integer> lang_drawable = new HashMap<Integer, Integer>();

    static {
        lang_list.add(0, "한국어");
        lang_list.add(1, "영어");
        lang_list.add(2, "중국어(광동어)");
        lang_list.add(3, "중국어(만다린)");

        lang_drawable.put(0, R.drawable.ic_lang_korean);
        lang_drawable.put(1, R.drawable.ic_lang_english);
        lang_drawable.put(2, R.drawable.ic_lang_chinese);
        lang_drawable.put(3, R.drawable.ic_lang_chinese);
    }

    public static ArrayList<String> getList() {
        return lang_list;
    }

    public static Integer getDrawable(Integer lang) {
        if(lang_drawable.containsKey(lang))
            return lang_drawable.get(lang);
        return null;
    }


    public static String getTitle(int lang) {
        return lang_list.get(lang);
    }
}
