package starlord.ciceron.mvp.common;

import java.util.HashMap;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 24..
 */
public class JobType {
    private static HashMap<Integer, String> jobtype_map_string = new HashMap<Integer, String>();
    private static HashMap<String, Integer> jobtype_map_drawable = new HashMap<String, Integer>();

    static {
        jobtype_map_drawable.put("TEXT", R.drawable.ic_text);
        jobtype_map_drawable.put("PHOTO", R.drawable.ic_photo);
        jobtype_map_drawable.put("AUDIO", R.drawable.ic_audio);
        jobtype_map_drawable.put("UPLOAD FILE", R.drawable.ic_file);

    }

    public static Integer getDrawable(String type) {
        if(jobtype_map_drawable.containsKey(type))
            return jobtype_map_drawable.get(type);
        return null;
    }

}
