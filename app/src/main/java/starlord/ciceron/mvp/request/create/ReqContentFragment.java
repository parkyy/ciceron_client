package starlord.ciceron.mvp.request.create;

import android.app.Activity;
import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.image.ImageLoader;
import starlord.ciceron.mvp.image.ImageWorker;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.request.common.BaseRequestFragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqContentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqContentFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqContentFragment extends BaseRequestFragment implements View.OnClickListener {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqContentFragment.
     */

    private TextView text_textSize;
    private EditText edit_content;
    private ImageView img_content;

    private ScrollView scroll_img;

    private Button btn_text;
    private Button btn_photo;
    private Button btn_gallery;

    private static int REQ_TYPE;
    private static final int REQ_TYPE_TEXT  = 100;
    private static final int REQ_TYPE_PHOTO = 101;
    private static final int REQ_TYPE_GALLERY = 102;
    private static final int REQ_TYPE_SOUND = 103;

    private String currentPhotoPath;
    private String currentSoundPath;

    private static final String DEBUG_TAG = "[ReqTextFragment]";

    public static ReqContentFragment newInstance() {
        ReqContentFragment fragment = new ReqContentFragment();
        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqContentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root =  inflater.inflate(R.layout.fragment_req_content, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        switchCurrentTabTo(REQ_TYPE_TEXT);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
        hideKeyboard();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(DEBUG_TAG, "onActivityResult");
        if(resultCode == Activity.RESULT_OK) {
            switch(requestCode) {

                case REQ_TYPE_PHOTO:
                    ImageWorker.galleryAddPic(getActivity(), currentPhotoPath);
                    setPhoto(currentPhotoPath, img_content);
                    break;

                case REQ_TYPE_GALLERY:
                    currentPhotoPath = ImageWorker.getPathFromURI(getActivity(), data.getData());
                    setPhoto(currentPhotoPath, img_content);
                    break;
            }
        }
        else {
            Toast.makeText(getActivity(), "result not ok", Toast.LENGTH_SHORT).show();
            currentPhotoPath = null;

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    protected void initView(View root) {
        btn_text  = (Button) root.findViewById(R.id.btn_typetext_content);
        btn_text.setOnClickListener(this);
        btn_photo = (Button) root.findViewById(R.id.btn_typephoto_content);
        btn_photo.setOnClickListener(this);
        btn_gallery  = (Button) root.findViewById(R.id.btn_typeimage_content);
        btn_gallery.setOnClickListener(this);

        scroll_img = (ScrollView) root.findViewById(R.id.scroll_req_content);
        img_content = (ImageView) root.findViewById(R.id.img_req_content);

        text_textSize = (TextView) root.findViewById(R.id.txt_req_textsize);
        text_textSize.setTypeface(Fonts.create(getActivity()).appleGothic());
        edit_content = (EditText) root.findViewById(R.id.edit_req_text);
        edit_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if(text_textSize != null)
                    text_textSize.setText(charSequence.length() + " words");
                invalidateOptionsMenu();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edit_content.setTypeface(Fonts.create(getActivity()).gotham());
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btn_typetext_content:
                switchCurrentTabTo(REQ_TYPE_TEXT);
                break;

            case R.id.btn_typephoto_content:
                switchCurrentTabTo(REQ_TYPE_PHOTO);
                dispatchTakePictureIntent();
                break;

            case R.id.btn_typeimage_content:
                switchCurrentTabTo(REQ_TYPE_GALLERY);
                dispatchGalleryPictureIntent();
                break;
        }
    }

    private void switchCurrentTabTo(final int type) {
        switch(type) {
            case REQ_TYPE_TEXT:
                REQ_TYPE = REQ_TYPE_TEXT;
                btn_text.setSelected(true);
                btn_photo.setSelected(false);
                btn_gallery.setSelected(false);
                showTextFrame();
                break;

            case REQ_TYPE_PHOTO:
                REQ_TYPE = REQ_TYPE_PHOTO;
                btn_text.setSelected(false);
                btn_photo.setSelected(true);
                btn_gallery.setSelected(false);
                showImageFrame();
                break;

            case REQ_TYPE_GALLERY:
                REQ_TYPE = REQ_TYPE_GALLERY;
                btn_text.setSelected(false);
                btn_photo.setSelected(false);
                btn_gallery.setSelected(true);
                showImageFrame();
                break;
        }
        //invalidateOptionsMenu();
    }

    private void showTextFrame() {
        hideEditText();
        hideImage();
        hideAudio();
        showEditText();
    }

    private void showImageFrame() {
        hideEditText();
        hideImage();
        hideAudio();
        showImage();
    }

    private void showAudioFrame() {
        hideEditText();
        hideImage();
        hideAudio();
        showAudio();
    }

    private void showEditText() {
        if (text_textSize != null && edit_content != null) {
            text_textSize.setVisibility(View.VISIBLE);
            edit_content.setVisibility(View.VISIBLE);
        }
    }

    private void hideEditText() {
        if (text_textSize != null && edit_content != null) {
            text_textSize.setVisibility(View.INVISIBLE);
            edit_content.setVisibility(View.INVISIBLE);
            edit_content.setText("");
        }
    }

    private void showImage() {
        if(scroll_img != null)
            scroll_img.setVisibility(View.VISIBLE);
    }

    private void hideImage() {
        if(scroll_img != null) {
            scroll_img.setVisibility(View.INVISIBLE);
            if (img_content != null) {
                img_content.setImageDrawable(null);
                currentPhotoPath = null;
                System.gc();
            }
        }
    }

    private void showAudio() {

    }

    private void hideAudio() {

    }

    private void dispatchGalleryPictureIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, REQ_TYPE_GALLERY);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = ImageWorker.createFile();

            // Continue only if the File was successfully created
            if (photoFile != null) {
                //contentUri = Uri.fromFile(photoFile);
                currentPhotoPath = photoFile.getAbsolutePath();
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQ_TYPE_PHOTO);
            } else  {
                Toast.makeText(getActivity(), "Unable to take picture", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void setPhoto(String filePath, ImageView imageView) {
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.loadImage(filePath, imageView);
        invalidateOptionsMenu();
    }

    @Override
    protected boolean isReadyToMoveOn() {
        if (REQ_TYPE == REQ_TYPE_TEXT) {
            if(edit_content != null && edit_content.length() > 0)
                return true;
            return false;
        } else if (REQ_TYPE == REQ_TYPE_PHOTO || REQ_TYPE == REQ_TYPE_GALLERY) {
            if(img_content != null && currentPhotoPath != null)
                return true;
            return false;
        } else {
            return false;
        }
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            switch (REQ_TYPE) {

                // TEXT TYPE REQEUST
                case REQ_TYPE_TEXT:
                    String text = edit_content.getText().toString();
                    mListener.submitType(Request.TYPE_TEXT);
                    mListener.submitContent(text);
                    mListener.submitWords(text.length());
                    mListener.submitPoints(text.length() * 100);
                    break;

                // IMAGE TYPE REQUEST
                case REQ_TYPE_PHOTO:
                    mListener.submitType(Request.TYPE_IMAGE);
                    mListener.submitContent(currentPhotoPath);
                    mListener.submitPoints(1000);
                    break;

                case REQ_TYPE_GALLERY:
                    mListener.submitType(Request.TYPE_IMAGE);
                    mListener.submitContent(currentPhotoPath);
                    mListener.submitPoints(1000);
                    break;

                // SOUND TYPE REQUEST
                case REQ_TYPE_SOUND:
                    mListener.submitType(Request.TYPE_SOUND);
                    mListener.submitContent(currentSoundPath);
                    mListener.submitPoints(1000);
                    break;

                default:
                    break;
            }
        }
    }

}
