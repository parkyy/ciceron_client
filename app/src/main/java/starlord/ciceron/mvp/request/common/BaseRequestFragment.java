package starlord.ciceron.mvp.request.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseRequestFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BaseRequestFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public abstract class BaseRequestFragment extends Fragment {

    protected OnRequestListener mListener;
    protected MenuItem menuNext;
    protected static Typeface font;
    protected int progressStage = 0;

    public BaseRequestFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnRequestListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnRequestInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mListener != null) {
            mListener.onStage();
        }
        setHasOptionsMenu(true);
        font = Fonts.create(getActivity()).gotham();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        View view = getView().findViewById(android.R.id.content);
        View view = getActivity().getWindow().getDecorView();
        setGlobalFont(view);
    }

    @Override
     public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_request, menu);
        menuNext = menu.findItem(R.id.btn_request_next);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home:
                if(mListener != null)
                    mListener.goBackToPreviousStage();
                break;

            case R.id.btn_request_next:
                if(mListener != null)
                    mListener.moveOnToNextStage();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    protected void hideKeyboard() {
        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    protected void setProgressStage(final int progressStage) {
        this.progressStage = progressStage;
    }

    protected int getStageNo() {
        return progressStage;
    }

    protected void  invalidateOptionsMenu() {
        if(isReadyToMoveOn()) {
            if (menuNext != null) {
                menuNext.setEnabled(true);
            }
        }
        else {
            if (menuNext != null) {
                menuNext.setEnabled(false);
            }
        }
    }

    abstract protected void    initView(View view);
    abstract protected boolean isReadyToMoveOn();
    abstract public void    submitRequestInfo();

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnRequestListener {
        public void   initRequest();
        public Bundle getRequestBundle();
        public void   onStage();
        public void   goBackToPreviousStage();
        public void   moveOnToNextStage();

        public void submitClientId(String value);
        public void submitClientName(String value);
        public void submitClientPicPath(String value);
        public void submitIsSos(boolean value);
        public void submitFormat(int value);
        public void submitSubject(int value);
        public void submitOriginalLang(int value);
        public void submitTargetLang(int value);
        public void submitType(int value);
        public void submitContent(String value);
        public void submitContext(String value);
        public void submitStatus(int value);
        public void submitRegisteredTime(String value);
        public void submitDueTime(String value);
        public void submitWords(int value);
        public void submitPoints(int value);

        public void onComplete();

    }

    private void setGlobalFont(View view) {
        if (view != null) {
            if(view instanceof ViewGroup){
                ViewGroup vg = (ViewGroup) view;
                int vgCnt = vg.getChildCount();
                for(int i=0; i < vgCnt; i++){
                    View v = vg.getChildAt(i);
                    if(v instanceof TextView){
                        ((TextView) v).setTypeface(font);
                    }
                    setGlobalFont(v);
                }
            }
        }
    }
}
