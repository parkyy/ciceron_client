package starlord.ciceron.mvp.request.create;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.ReqFormatAdapter;
import starlord.ciceron.mvp.request.common.BaseRequestFragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqFormatFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqFormatFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqFormatFragment extends BaseRequestFragment {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqContentFragment.
     */

    private GridView grid_format;
    private int      reqFormat = -1;

    private static final String DEBUG_TAG = "[ReqFormatFragment]";

    public static ReqFormatFragment newInstance() {
        ReqFormatFragment fragment = new ReqFormatFragment();
        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    // Required empty public constructor
    public ReqFormatFragment() { reqFormat = -1; }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root =  inflater.inflate(R.layout.fragment_req_format2, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        if(savedInstanceState != null) {
            reqFormat = savedInstanceState.getInt("format");
            chooseFormat(reqFormat);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(DEBUG_TAG, "onSaveInstanceState");
        if(reqFormat >= 0) {
            outState.putInt("format", reqFormat);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(reqFormat >= 0)
            chooseFormat(reqFormat);
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    protected void initView(View view) {
        grid_format = (GridView) view.findViewById(R.id.grid_req_format);
        grid_format.setAdapter(new ReqFormatAdapter(getActivity()));
        grid_format.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                chooseFormat(index);
            }
        });
    }

    private void chooseFormat(int index) {
        reqFormat = index;
        if (grid_format != null) {
            grid_format.requestFocusFromTouch();
            grid_format.setSelection(index);
        }
        invalidateOptionsMenu();
    }

    @Override
    protected boolean isReadyToMoveOn() {
        if(reqFormat >= 0)
            return true;
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            mListener.submitFormat(reqFormat);
        }
    }

}
