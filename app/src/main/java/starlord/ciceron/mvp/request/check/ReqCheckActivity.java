package starlord.ciceron.mvp.request.check;

import android.os.Bundle;
import android.view.MenuItem;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.request.common.ReqOverview;
import starlord.ciceron.mvp.translate.TranslateOverview;

public class ReqCheckActivity extends BaseActivity {
    private Bundle reqBundle;

    private ReqCheckFragment1 reqCheckFragment1;
    private ReqCheckFragment2 reqCheckFragment2;
    private ReqCheckFragment3 reqCheckFragment3;

    private TranslateOverview translateView;
    private ReqOverview requestView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_req_check);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("Request Detail");
/*
        if(getIntent() != null) {
            reqBundle = getIntent().getExtras();
            Bundle transBundle = reqBundle.getBundle("transBundle");

            translateView = (TranslateOverview) findViewById(R.id.view_translate_req_check);
            translateView.setLayout(transBundle);

            requestView = (ReqOverview) findViewById(R.id.view_request_req_check);
            requestView.setLayout(reqBundle);
        }*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    
}
