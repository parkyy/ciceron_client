package starlord.ciceron.mvp.request.create;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.view.View.OnClickListener;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.request.common.BaseRequestFragment;
import starlord.ciceron.mvp.request.common.ReqOverview;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqReviewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqReviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqReviewFragment extends BaseRequestFragment {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqReviewFragment.
     */
    private Bundle          reqBundle;
    private ReqOverview requestView;
    private Button          btnComplete;

    private static final String DEBUG_TAG = "[ReqReviewFragment]";

    public static ReqReviewFragment newInstance() {
        ReqReviewFragment fragment = new ReqReviewFragment();
        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqReviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reqBundle = mListener.getRequestBundle();
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_req_review2, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    //    setLayout();
        Log.d(DEBUG_TAG, "onActivityCreated");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuNext.setVisible(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    protected void initView(View root) {
        requestView = (ReqOverview) root.findViewById(R.id.view_request_req_review);
        requestView.setLayout(reqBundle);

        btnComplete   = (Button) root.findViewById(R.id.btn_complete_req_review);
        btnComplete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                submitRequestInfo();
            }
        });
    }

    @Override
    protected boolean isReadyToMoveOn() {
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            mListener.onComplete();
        }
    }
}
