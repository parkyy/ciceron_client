package starlord.ciceron.mvp.request.create;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.ReqSubjectAdapter;
import starlord.ciceron.mvp.request.common.BaseRequestFragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqSubjectFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqSubjectFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqSubjectFragment extends BaseRequestFragment {

    /*
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment ReqInfoFragment.
         */
    private GridView  grid_subject;
    private int       reqSubject;

    private static final String DEBUG_TAG = "[ReqSubjectFragment]";

    public static ReqSubjectFragment newInstance() {
        ReqSubjectFragment fragment = new ReqSubjectFragment();
        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    // Required empty public constructor
    public ReqSubjectFragment() {
        reqSubject = -1;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root =  inflater.inflate(R.layout.fragment_req_subject2, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        if(savedInstanceState != null) {
            reqSubject = savedInstanceState.getInt("subject");
            chooseSubject(reqSubject);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(DEBUG_TAG, "onSaveInstanceState");
        if(reqSubject >= 0) {
            outState.putInt("subject", reqSubject);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(reqSubject >=0) {
            chooseSubject(reqSubject);
        }
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");

    }

    @Override
    protected void initView(View view) {
        grid_subject = (GridView) view.findViewById(R.id.grid_req_subject);
        grid_subject.setAdapter(new ReqSubjectAdapter(getActivity()));
        grid_subject.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        grid_subject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                chooseSubject(index);
            }
        });
    }


    public void chooseSubject(int index) {
        reqSubject = index;
        if(grid_subject != null) {
           grid_subject.requestFocusFromTouch();
           grid_subject.setSelection(index);
        }
        invalidateOptionsMenu();
    }

    protected boolean isReadyToMoveOn() {
        if(reqSubject >= 0)
            return true;
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            mListener.initRequest();
            mListener.submitSubject(reqSubject);
        }
    }
}
