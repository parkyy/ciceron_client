package starlord.ciceron.mvp.request.common;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 5. 30..
 */
public class ReqSosView extends RelativeLayout {

    private Context context;


    private EditText edit_content;
    private TextView text_words;


    public ReqSosView(Context context) {
        super(context);
        initLayout(context);
    }

    public ReqSosView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }


    private void initLayout(Context context) {
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_req_sos, this, true);

        edit_content = (EditText) findViewById(R.id.edit_content_reqSosView);
        edit_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if(text_words != null)
                    text_words.setText(charSequence.length() + "/140");
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        text_words = (TextView) findViewById(R.id.text_words_reqSosView);
    }

}
