package starlord.ciceron.mvp.request.create;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Language;
import starlord.ciceron.mvp.collection.LanguageAdapter;
import starlord.ciceron.mvp.request.common.BaseRequestFragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqLangFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqLangFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ReqLangFragment extends BaseRequestFragment implements OnClickListener {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReqInfoFragment.
     */

    private ImageView img_originalLang;
    private ImageView img_targetLang;
    private ImageView img_switch;

    private TextView  text_originalLang;
    private TextView  text_targetLang;

    private int       originalLang;
    private int       targetLang;
    private int       whichLang;

    private LanguageAdapter langAdapter;

    private static final String DEBUG_TAG = "[ReqLangFragment]";

    public static ReqLangFragment newInstance() {
        ReqLangFragment fragment = new ReqLangFragment();
        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    // Required empty public constructor
    public ReqLangFragment() {
        originalLang = -1;
        targetLang = -1;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        langAdapter = new LanguageAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_req_lang, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        if(savedInstanceState != null) {
            originalLang = savedInstanceState.getInt("originalLang");
            targetLang = savedInstanceState.getInt("targetLang");
            selectLanguage(originalLang);
            selectTargetLang(targetLang);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(DEBUG_TAG, "onSaveInstanceState");
        if(originalLang >= 0 && targetLang >= 0) {
            outState.putInt("originalLang", originalLang);
            outState.putInt("targetLang", targetLang);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
        if(originalLang >=0 && targetLang >=0) {
            selectOriginalLang(originalLang);
            selectTargetLang(targetLang);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    protected void initView(View view) {
        img_originalLang = (ImageView) view.findViewById(R.id.btn_fromlang_req_lang);
        img_targetLang = (ImageView) view.findViewById(R.id.btn_tolang_req_lang);
        img_switch = (ImageView) view.findViewById(R.id.btn_switchlang_req_lang);

        img_originalLang.setOnClickListener(this);
        img_targetLang.setOnClickListener(this);
        img_switch.setOnClickListener(this);

        text_originalLang = (TextView) view.findViewById(R.id.text_fromlang_req_lang);
        text_targetLang = (TextView) view.findViewById(R.id.text_tolang_req_lang);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_fromlang_req_lang:
                whichLang = R.id.btn_fromlang_req_lang;
                break;

            case R.id.btn_tolang_req_lang:
                whichLang = R.id.btn_tolang_req_lang;
                break;

            case R.id.btn_switchlang_req_lang:
                switchLanguage();
                return;
        }
        showDialog();
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setTitle(getActivity().getResources().getString(R.string.title_req_lang))
                .setAdapter(langAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        selectLanguage(position);
                        dialogInterface.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                invalidateOptionsMenu();
            }
        });
        dialog.show();
    }

    private void selectLanguage(int which) {
        if(whichLang == R.id.btn_fromlang_req_lang) {
            selectOriginalLang(which);
        } else if(whichLang == R.id.btn_tolang_req_lang) {
            selectTargetLang(which);
        }
    }

    private void selectOriginalLang(int lang) {
        originalLang = lang;
        if(text_originalLang != null)
            text_originalLang.setText(langAdapter.getItem(lang));
        if(img_originalLang != null)
            img_originalLang.setImageResource(Language.getDrawable(lang));
        invalidateOptionsMenu();
    }

    private void selectTargetLang(int lang) {
        targetLang = lang;
        if(text_targetLang != null)
            text_targetLang.setText(langAdapter.getItem(lang));
        if(img_originalLang != null)
            img_targetLang.setImageResource(Language.getDrawable(lang));
        invalidateOptionsMenu();
    }

    private void switchLanguage() {
        if(isReadyToMoveOn()) {
            int temp = originalLang;
            originalLang = targetLang;
            targetLang = temp;

            selectOriginalLang(originalLang);
            selectTargetLang(targetLang);
        }
    }

    @Override
    protected boolean isReadyToMoveOn() {
        if(originalLang >= 0 && targetLang >= 0)
            return true;
        return false;
    }

    @Override
    public void submitRequestInfo() {
        if(mListener != null) {
            mListener.submitOriginalLang(originalLang);
            mListener.submitTargetLang(targetLang);
        }
    }
}
