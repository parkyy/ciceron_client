package starlord.ciceron.mvp.request.check;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.NewsFeedList;
import starlord.ciceron.mvp.collection.ReqPending;
import starlord.ciceron.mvp.request.common.ReqOverview;

public class ReqEditActivity extends BaseActivity {
    private Bundle    reqBundle;
    private ReqOverview requestView;
    private Button    btnDelete;

    private static final String    DEBUG_TAG = "[ReqEditActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_req_edit);
        Log.d(DEBUG_TAG, "onCreate");

        getActionBar().setDisplayHomeAsUpEnabled(true);

        reqBundle = getIntent().getExtras();
        requestView = (ReqOverview) findViewById(R.id.view_request_req_edit);
        requestView.setLayout(reqBundle);

        btnDelete     = (Button)   findViewById(R.id.btn_delete_edit);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.req_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(getResources().getString(R.string.dialog_req_delete))
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        deleteRequest();
                    }
                })
                .setNegativeButton("아니오", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.create().show();
    }

    private void deleteRequest() {
        if(reqBundle != null) {
            int id = reqBundle.getInt("id");
            ReqPending.removeRequest(id);
            NewsFeedList.removeRequest(id);
        }
        finish();
    }
}
