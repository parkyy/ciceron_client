package starlord.ciceron.mvp.request.create;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.NewsFeedList;
import starlord.ciceron.mvp.collection.ReqPending;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.request.common.BaseRequestFragment;

public class ReqMainActivity extends BaseActivity implements BaseRequestFragment.OnRequestListener {

    private FragmentManager         fm;
    private ProgressBar             pb;
    private Bundle                  reqBundle;

    private BaseRequestFragment[]   reqStageArray;
    private ReqSubjectFragment      reqSubjectFrag;
    private ReqFormatFragment       reqFormatFrag;
    private ReqLangFragment         reqLangFrag;
    private ReqContentFragment      reqContentFrag;
    private ReqContextFragment      reqContextFrag;
    private ReqTimeFragment         reqTimeFrag;
    private ReqReviewFragment       reqReviewFrag;



    private static final CharSequence[] stageTitle = {
                                                 "Choose Subject",
                                                 "Set Format",
                                                 "Set Language",
                                                 "Type in Text",
                                                 "Context",
                                                 "Check Duedate",
                                                 "Review Order" };
    private int reqStage = 0;
    private static final String    DEBUG_TAG = "[RequestActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_req_main);
        Log.d(DEBUG_TAG, "onCreate");

        initRequestActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();

        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(DEBUG_TAG, "onDestroy");
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void initRequestActivity() {
        fm = getFragmentManager();
        pb = (ProgressBar) findViewById(R.id.pb_request);
        pb.setProgress(reqStage * 16);
        actionBar.setDisplayHomeAsUpEnabled(true);
        initRequest();

        reqStageArray = new BaseRequestFragment[7];
        reqStageArray[0] = ReqFormatFragment.newInstance();
        reqStageArray[1] = ReqSubjectFragment.newInstance();
        reqStageArray[2] = ReqContentFragment.newInstance();
        reqStageArray[3] = ReqLangFragment.newInstance();
        reqStageArray[4] = ReqTimeFragment.newInstance();
        reqStageArray[5] = ReqContextFragment.newInstance();
        reqStageArray[6] = ReqReviewFragment.newInstance();

        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_request, reqStageArray[0]);
        ft.commit();
    }

    @Override
    public void initRequest() {
        if(reqBundle == null)
            reqBundle = new Bundle();

        //TODO: modify this

        submitClientId("pjh0308@gmail.com");
        submitClientName("Jaehong Park");
        submitClientPicPath("http://52.11.126.237:5000");
        submitRegisteredTime("2015.05.25");
        submitStatus(Request.STATUS_PENDING);

        //TODO : MODIFY THIS
    }

    @Override
    public Bundle getRequestBundle() {
        return reqBundle;
    }

    @Override
    public void onStage() {
        if(pb != null)
            pb.setProgress(reqStage * 16);
    }

    @Override
    public void goBackToPreviousStage() {
        if (reqStage == 0) {
            onBackPressed();
            return;
        }
        FragmentTransaction ft = fm.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
        ft.replace(R.id.frame_request, reqStageArray[--reqStage]);
        ft.commit();
    }

    @Override
    public void moveOnToNextStage() {
        if (reqStage < reqStageArray.length - 1) {
            reqStageArray[reqStage].submitRequestInfo();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frame_request, reqStageArray[++reqStage]);
            ft.commit();
        }
    }

    @Override
    public void submitClientId(String value) {
        if(reqBundle != null) {
            reqBundle.putString("clientId", value);
            Log.d(DEBUG_TAG, "clientId: " + String.valueOf(value));
        }
    }

    @Override
    public void submitClientName(String value) {
        if(reqBundle != null) {
            reqBundle.putString("clientName", value);
            Log.d(DEBUG_TAG, "clientName: " + String.valueOf(value));
        }
    }

    @Override
    public void submitClientPicPath(String value) {
        if(reqBundle != null) {
            reqBundle.putString("clientPicPath", value);
            Log.d(DEBUG_TAG, "clientPicPath: " + String.valueOf(value));
        }
    }

    @Override
    public void submitIsSos(boolean value) {
        if(reqBundle != null) {
            reqBundle.putBoolean("isSos", value);
            Log.d(DEBUG_TAG, "isSos: " + String.valueOf(value));
        }
    }

    @Override
    public void submitFormat(int value) {
        if(reqBundle != null) {
            reqBundle.putInt("format", value);
            Log.d(DEBUG_TAG, "format: " + String.valueOf(value));
        }
    }

    @Override
    public void submitSubject(int value) {
        if(reqBundle != null) {
            reqBundle.putInt("subject", value);
            Log.d(DEBUG_TAG, "subject: " + String.valueOf(value));
        }
    }

    @Override
    public void submitOriginalLang(int value) {
        if(reqBundle != null) {
            reqBundle.putInt("originalLang", value);
            Log.d(DEBUG_TAG, "originalLang: " + String.valueOf(value));
        }
    }

    @Override
    public void submitTargetLang(int value) {
        if(reqBundle != null) {
            reqBundle.putInt("targetLang", value);
            Log.d(DEBUG_TAG, "targetLang: " + String.valueOf(value));
        }
    }

    @Override
    public void submitType(int value) {
        if(reqBundle != null) {
            reqBundle.putInt("type", value);
            Log.d(DEBUG_TAG, "type: " + String.valueOf(value));
        }
    }

    @Override
    public void submitContent(String value) {
        if(reqBundle != null) {
            reqBundle.putString("content", value);
            Log.d(DEBUG_TAG, "content: " + String.valueOf(value));
        }
    }

    @Override
    public void submitContext(String value) {
        if(reqBundle != null) {
            reqBundle.putString("context", value);
            Log.d(DEBUG_TAG, "context: " + String.valueOf(value));
        }
    }

    @Override
    public void submitStatus(int value) {
        if(reqBundle != null) {
            reqBundle.putInt("status", value);
            Log.d(DEBUG_TAG, "status: " + String.valueOf(value));
        }
    }

    @Override
    public void submitRegisteredTime(String value) {
        if(reqBundle != null) {
            reqBundle.putString("registeredTime", value);
            Log.d(DEBUG_TAG, "registeredTime: " + String.valueOf(value));
        }
    }

    @Override
    public void submitDueTime(String value) {
        if(reqBundle != null) {
            reqBundle.putString("dueTime", value);
            Log.d(DEBUG_TAG, "dueTime: " + String.valueOf(value));
        }
    }

    @Override
    public void submitWords(int value) {
        if(reqBundle != null) {
            reqBundle.putInt("words", value);
            Log.d(DEBUG_TAG, "words: " + String.valueOf(value));
        }
    }

    @Override
    public void submitPoints(int value) {
        if(reqBundle != null) {
            reqBundle.putInt("points", value);
            Log.d(DEBUG_TAG, "points: " + String.valueOf(value));
        }
    }


    @Override
    public void onComplete() {

        Request request = Request.fromBundle(reqBundle);
        if(request.isValid()) {

            //TODO : MODIFY THIS
            ReqPending.addRequest(request);
            NewsFeedList.addRequest(request);
            Toast.makeText(getBaseContext(), "Request Complete", Toast.LENGTH_SHORT).show();

            //TODO : MODIFY THIS
        }
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onBackPressed() {
        showCancelDialog();
    }

    private void showCancelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("의뢰물 작성을 취소하실 건가요?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    }
                })
                .setNegativeButton("이전으로", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.create().show();
    }
}
