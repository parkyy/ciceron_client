package starlord.ciceron.mvp.request.create;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.request.common.BaseRequestFragment;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReqTimeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReqTimeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReqTimeFragment extends BaseRequestFragment {

    private DatePicker datePicker;
    private TimePicker timePicker;

    private int dueYear;
    private int dueMonth;
    private int dueDay;
    private int dueHour;
    private int dueMinute;

    private static final String DEBUG_TAG = "[ReqTimeFragment]";

    public static ReqTimeFragment newInstance() {
        ReqTimeFragment fragment = new ReqTimeFragment();
        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public ReqTimeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        Log.d(DEBUG_TAG, "onCreateView");
        View root =  inflater.inflate(R.layout.fragment_req_time, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");

        Handler mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                invalidateOptionsMenu();
            }
        }, 500);
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    protected void initView(View root) {
        datePicker = (DatePicker) root.findViewById(R.id.datePicker_req_time);
        datePicker.init(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(),
                        new DatePicker.OnDateChangedListener() {
                            @Override
                            public void onDateChanged(DatePicker datePicker, int year, int month, int day) {
                                dueYear = year;
                                dueMonth = month + 1;
                                dueDay = day;
                            }
                        });
        timePicker = (TimePicker) root.findViewById(R.id.timePicker_req_time);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker timePicker, int hour, int minute) {
                dueHour = hour;
                dueMinute = minute;
            }
        });
    }


    @Override
    protected boolean isReadyToMoveOn() {
        return true;
    }

    @Override
    public void submitRequestInfo() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, datePicker.getYear());
        cal.set(Calendar.MONTH, datePicker.getMonth());
        cal.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
        cal.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
        cal.set(Calendar.MINUTE, timePicker.getCurrentMinute());


        Date dueTime = cal.getTime();
        Date registeredTime = new Date(System.currentTimeMillis());

        if(mListener != null) {
            mListener.submitRegisteredTime(dateFormat.format(registeredTime));
            mListener.submitDueTime(dateFormat.format(dueTime));
        }
    }
}
