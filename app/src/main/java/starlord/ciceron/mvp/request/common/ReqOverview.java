package starlord.ciceron.mvp.request.common;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.common.Format;
import starlord.ciceron.mvp.common.Language;
import starlord.ciceron.mvp.common.Subject;
import starlord.ciceron.mvp.image.ImageLoader;
import starlord.ciceron.mvp.collection.Request;

/**
 * Created by JaehongPark on 15. 1. 10..
 */
public class ReqOverview extends RelativeLayout {
    private Context   context;
    private Bundle    reqBundle;

    private ImageView img_content;
    private ImageView img_originalLang;
    private ImageView img_targetLang;
    private ImageView img_format;
    private ImageView img_subject;

    private TextView  text_type;
    private TextView  text_registeredTime;
    private TextView  text_content;
    private TextView  text_context;
    private TextView  text_originalLang;
    private TextView  text_targetLang;
    private TextView  text_format;
    private TextView  text_subject;
    private TextView  text_dueTime;
    private TextView  text_words;
    private TextView  text_price;

    public ReqOverview(Context context) {
        super(context);
        initLayout(context);
    }

    public ReqOverview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public ReqOverview(Context context, Bundle reqBundle) {
        super(context);
        initLayout(context);
    }

    public ReqOverview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout(context);
    }

    public void initLayout(Context context) {
        this.context = context;

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_req_overview2, this, true);

        img_content = (ImageView) findViewById(R.id.img_content_reqOverview);
        img_originalLang = (ImageView) findViewById(R.id.img_originLang_reqOverview);
        img_targetLang = (ImageView) findViewById(R.id.img_targetLang_reqOverview);
        img_format = (ImageView) findViewById(R.id.img_format_reqOverview);
        img_subject = (ImageView) findViewById(R.id.img_subject_reqOverview);

        text_type = (TextView) findViewById(R.id.text_titleContent_reqOverview);
        text_type.setTypeface(Fonts.create(context).appleGothicBold());
        text_registeredTime = (TextView) findViewById(R.id.text_regTime_reqOverview);
        text_registeredTime.setTypeface(Fonts.create(context).appleGothicBold());
        text_content = (TextView) findViewById(R.id.text_content_reqOverview);
        text_content.setTypeface(Fonts.create(context).appleGothicBold());
        text_context = (TextView) findViewById(R.id.text_context_reqOverview);
        text_context.setTypeface(Fonts.create(context).appleGothicBold());
        text_originalLang = (TextView) findViewById(R.id.text_originLang_reqOverview);
        text_originalLang.setTypeface(Fonts.create(context).appleGothicBold());
        text_targetLang = (TextView) findViewById(R.id.text_targetLang_reqOverview);
        text_targetLang.setTypeface(Fonts.create(context).appleGothicBold());
        text_format = (TextView) findViewById(R.id.text_format_reqOverview);
        text_format.setTypeface(Fonts.create(context).appleGothicBold());
        text_subject = (TextView) findViewById(R.id.text_subject_reqOverview);
        text_subject.setTypeface(Fonts.create(context).appleGothicBold());
        text_words = (TextView) findViewById(R.id.text_words_reqOverview);
        text_words.setTypeface(Fonts.create(context).appleGothicBold());
        text_dueTime = (TextView) findViewById(R.id.text_dueTime_reqOverview);
        text_dueTime.setTypeface(Fonts.create(context).appleGothicBold());
        text_price = (TextView) findViewById(R.id.text_price_reqOverview);
        text_price.setTypeface(Fonts.create(context).appleGothicBold());

    }

    public void setLayout(Request request) {
        if(request != null) {
            int originalLang = request.originalLang;
            int targetLang = request.targetLang;
            int format = request.format;
            int subject = request.subject;
            int type   = request.type;
            int words = request.words;
            int points = request.points;

            String registeredTime = request.registeredTime;
            String dueTime = request.dueTime;
            String content = request.content;
            String context = request.context;

            img_originalLang.setImageResource(Language.getDrawable(originalLang));
            img_targetLang.setImageResource(Language.getDrawable(targetLang));

            img_format.setImageResource(Format.getDrawable(format));
            img_subject.setImageResource(Subject.getDrawable(subject));

            text_originalLang.setText(Language.getTitle(originalLang));
            text_targetLang.setText(Language.getTitle(targetLang));

            text_format.setText(Format.getTitle(format));
            text_subject.setText(Subject.getTitle(subject));

            text_registeredTime.setText(registeredTime);
            text_dueTime.setText(dueTime);
            text_words.setText(String.valueOf(words));
            text_price.setText(String.valueOf(points));
            text_context.setText(context);

            if (type == Request.TYPE_TEXT) {

                text_type.setText("Text");
                text_content.setText(content);
                img_content.setVisibility(View.GONE);

            } else if (type == Request.TYPE_IMAGE) {

                text_type.setText("Photo");
                text_content.setVisibility(View.INVISIBLE);

                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.loadImage(content, img_content);

            }
        }
    }

    public void setLayout(Bundle reqBundle) {

        if(reqBundle != null) {
            Integer originalLang = reqBundle.getInt("originalLang");
            Integer targetLang = reqBundle.getInt("targetLang");
            Integer format = reqBundle.getInt("format");
            Integer subject = reqBundle.getInt("subject");
            Integer type   = reqBundle.getInt("type");
            Integer words = reqBundle.getInt("words");
            Integer points = reqBundle.getInt("points");

            String registeredTime = reqBundle.getString("registeredTime");
            String dueTime = reqBundle.getString("dueTime");
            String content = reqBundle.getString("content");
            String context = reqBundle.getString("context");

            img_originalLang.setImageResource(Language.getDrawable(originalLang));
            img_targetLang.setImageResource(Language.getDrawable(targetLang));

            img_format.setImageResource(Format.getDrawable(format));
            img_subject.setImageResource(Subject.getDrawable(subject));

            text_originalLang.setText(Language.getTitle(originalLang));
            text_targetLang.setText(Language.getTitle(targetLang));

            text_format.setText(Format.getTitle(format));
            text_subject.setText(Subject.getTitle(subject));

            text_registeredTime.setText(registeredTime);
            text_dueTime.setText(dueTime);
            text_words.setText(words.toString());
            text_price.setText(points.toString());

            text_context.setText(context);

            if( type == Request.TYPE_TEXT ) {
//                text_type.setText("Text");
                text_content.setText(content);
                img_content.setVisibility(View.GONE);
            }
            else if( type == Request.TYPE_IMAGE ) {
//                text_type.setText("Photo");
                text_content.setVisibility(View.INVISIBLE);

                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.loadImage(content, img_content);

            }
        }
    }

    public void hideContent() {
        if(text_content == null || img_content == null)
            return;
        text_type.setVisibility(View.GONE);
        text_content.setVisibility(View.GONE);
        img_content.setVisibility(View.GONE);
    }

}
