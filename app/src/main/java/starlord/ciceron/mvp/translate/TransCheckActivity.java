package starlord.ciceron.mvp.translate;

import android.os.Bundle;
import android.view.MenuItem;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.request.common.ReqOverview;

public class TransCheckActivity extends BaseActivity {
    private Bundle reqBundle;

    private TranslateOverview translateView;
    private ReqOverview requestView;

    private static final String DEBUG_TAG = "[TransCheckActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans_check);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setTitle("Translation Detail");

        if(getIntent() != null) {
            reqBundle = getIntent().getExtras();
            Bundle transBundle = reqBundle.getBundle("transBundle");

            translateView = (TranslateOverview) findViewById(R.id.view_translate_trans_check);
            translateView.setLayout(transBundle);

            requestView = (ReqOverview) findViewById(R.id.view_request_trans_check);
            requestView.setLayout(reqBundle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
