package starlord.ciceron.mvp.translate;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BaseTransFragment.OnTranslationListener} interface
 * to handle interaction events.
 *
 *
 */
public abstract class BaseTransFragment extends Fragment {

    protected int progressStage = 0;
    protected MenuItem menuNext;
    protected OnTranslationListener mListener;
    private static Typeface font;

    public BaseTransFragment() { }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnTranslationListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnTransInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mListener != null) {
            mListener.onStage(progressStage);
        }
        setHasOptionsMenu(true);
        font = Fonts.create(getActivity()).gotham();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        View view = getActivity().getWindow().getDecorView();
        setGlobalFont(view);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_translate, menu);
        menuNext = menu.findItem(R.id.btn_translate_nextstep);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home:
                if(mListener != null)
                    mListener.goBackToPreviousStage();
                break;

            case R.id.btn_translate_nextstep:
                if(mListener != null)
                    mListener.moveOnToNextStage();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    protected void setProgressStage(int progressStage) {
        this.progressStage = progressStage;
    }
    protected int getStageNum() {
        return progressStage;
    }

    protected void invalidateOptionsMenu() {
        if(isReadyToMoveOn()) {
            if (menuNext != null) {
                menuNext.setEnabled(true);
            }
        }
        else {
            if (menuNext != null) {
                menuNext.setEnabled(false);
            }
        }
    }

    abstract protected void initView(View root);
    abstract protected boolean isReadyToMoveOn();
    abstract protected void submitTranslationInfo();

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnTranslationListener {
        public void   initTranslation();
        public Bundle getRequestBundle();
        public Bundle getTranslationBundle();
        public void   onStage(int step);
        public void   goBackToPreviousStage();
        public void   moveOnToNextStage();

        public void submitTranslatorId(String value);
        public void submitTranslatorName(String value);
        public void submitTranslatorPicPath(String value);
        public void submitTranslationText(String value);
        public void submitTranslationComment(String value);
        public void submitTranslationTime(String value);
        public void submitTranslationTone(int value);

        public void onComplete();
    }

    private void setGlobalFont(View view) {
        if (view != null) {
            if(view instanceof ViewGroup){
                ViewGroup vg = (ViewGroup) view;
                int vgCnt = vg.getChildCount();
                for(int i=0; i < vgCnt; i++){
                    View v = vg.getChildAt(i);
                    if(v instanceof TextView){
                        ((TextView) v).setTypeface(font);
                    }
                    setGlobalFont(v);
                }
            }
        }
    }
}
