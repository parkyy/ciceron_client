package starlord.ciceron.mvp.translate;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.HttpURLConnection;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.network.HttpConnManager;
import starlord.ciceron.mvp.network.NetResponse;
import starlord.ciceron.mvp.network.NetworkAPI;

public class WorkerMainActivity extends Activity {

    private static final String DEBUG_TAG = "[WorkerMainActivity]";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_worker_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.btn_menu_worker_main) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private int executePostSos(final String request) {

        OkHttpClient client = HttpConnManager.getInstance();
        int statusCode = -1;

/*        RequestBody body = new FormEncodingBuilder()
                .add("email", email)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .url(NetworkConst.LOGIN_API)
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Accept", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .build();
        try {
            Response response = client.newCall(request).execute();
            statusCode = response.code();
            Log.d(DEBUG_TAG, "postSosResponse:" + response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return statusCode;
    }

    private int executeAddToQueue(final int requestId) {
        OkHttpClient client = HttpConnManager.getInstance();
        int statusCode = 0;

        RequestBody body = new FormEncodingBuilder()
                .add("request_id", String.valueOf(requestId))
                .build();

        Request request = new Request.Builder()
                .url(NetworkAPI.USER_TRANSLATION_PENDING_API)
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Accept", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .build();
        try {
            Response response = client.newCall(request).execute();
            switch(response.code()) {
                case HttpURLConnection.HTTP_OK:
                    handleHttpResponse(NetResponse.ADD_QUEUE_OK);
                    break;

                case HttpURLConnection.HTTP_FORBIDDEN:
                    handleHttpResponse(NetResponse.NO_TRANSLATOR_AUTH);
                    break;

                case HttpURLConnection.HTTP_NOT_ACCEPTABLE:
                    handleHttpResponse(NetResponse.NOT_ACCESSABLE_REQ);
                    break;

                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    handleHttpResponse(NetResponse.NO_LANG_PERMISSION);
                    break;

                case HttpURLConnection.HTTP_NO_CONTENT:
                    handleHttpResponse(NetResponse.ALREADY_IN_QUEUE);
                    break;

                default:
                    handleHttpResponse(NetResponse.OTHERS);
                    break;

            }
            Log.d(DEBUG_TAG, "addToQueueResponse:" + response.toString());
        } catch (IOException e) {
            handleHttpResponse(NetResponse.OTHERS);
            e.printStackTrace();
        }
        return statusCode;
    }

    private int executeInitTransWork(final int requestId) {
        OkHttpClient client = HttpConnManager.getInstance();
        int statusCode = 0;

        RequestBody body = new FormEncodingBuilder()
                .add("request_id", String.valueOf(requestId))
                .build();

        Request request = new Request.Builder()
                .url(NetworkAPI.USER_TRANSLATION_ONGOING_API)
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Accept", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .build();
        try {
            Response response = client.newCall(request).execute();
            switch(response.code()) {
                case HttpURLConnection.HTTP_OK:
                    handleHttpResponse(NetResponse.INIT_TRANS_OK);
                    break;

                case HttpURLConnection.HTTP_FORBIDDEN:
                    handleHttpResponse(NetResponse.NO_TRANSLATOR_AUTH);
                    break;

                case HttpURLConnection.HTTP_NOT_ACCEPTABLE:
                    handleHttpResponse(NetResponse.NOT_ACCESSABLE_REQ);
                    break;

                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    handleHttpResponse(NetResponse.NO_LANG_PERMISSION);
                    break;

                case HttpURLConnection.HTTP_GONE:
                    handleHttpResponse(NetResponse.ALREADY_TAKEN_REQ);
                    break;

                default:
                    handleHttpResponse(NetResponse.OTHERS);
                    break;

            }
            Log.d(DEBUG_TAG, "initTransWorkResponse:" + response.toString());
        } catch (IOException e) {
            handleHttpResponse(NetResponse.OTHERS);
            e.printStackTrace();
        }
        return statusCode;
    }

    private int executeDeleteRequest(final int requestId) {
        OkHttpClient client = HttpConnManager.getInstance();
        int statusCode = 0;
        String url = "/" + String.valueOf(requestId);

        Request request = new Request.Builder()
                .url(NetworkAPI.USER_TRANSLATION_PENDING_API + url)
                .delete()
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Accept", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .build();
        try {
            Response response = client.newCall(request).execute();
            switch(response.code()) {
                case HttpURLConnection.HTTP_OK:
                    handleHttpResponse(NetResponse.DEL_REQUEST_OK);
                    break;

                default:
                    handleHttpResponse(NetResponse.OTHERS);
                    break;

            }
            Log.d(DEBUG_TAG, "delRequestResponse:" + response.toString());
        } catch (IOException e) {
            handleHttpResponse(NetResponse.OTHERS);
            e.printStackTrace();
        }
        return statusCode;
    }



    private void handleHttpResponse(int response) {
        switch(response) {
            case NetResponse.ADD_QUEUE_OK:
                break;

            case NetResponse.NO_TRANSLATOR_AUTH:
                break;

            case NetResponse.NO_LANG_PERMISSION:
                break;

            case NetResponse.NOT_ACCESSABLE_REQ:
                break;

            case NetResponse.INIT_TRANS_OK:
                break;

            case NetResponse.ALREADY_IN_QUEUE:
                break;

            case NetResponse.ALREADY_TAKEN_REQ:
                break;

            case NetResponse.DEL_REQUEST_OK:
                break;

            case NetResponse.OTHERS:
                break;
        }
    }
}
