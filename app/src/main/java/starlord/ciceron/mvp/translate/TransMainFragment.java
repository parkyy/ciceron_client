package starlord.ciceron.mvp.translate;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import starlord.ciceron.mvp.request.common.ReqOverview;
import starlord.ciceron.mvp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransMainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransMainFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TransMainFragment extends BaseTransFragment {
    private Bundle   reqBundle;

    private EditText edit_content;
    private ReqOverview requestView;

    private static final String DEBUG_TAG = "[TransMainFragment]";

    public static TransMainFragment newInstance(int stageNum) {
        TransMainFragment fragment = new TransMainFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("progressStage", stageNum);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public TransMainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            progressStage = getArguments().getInt("progressStage");
        }
        reqBundle = mListener.getRequestBundle();
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_trans_main, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        edit_content.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                manager.showSoftInput(edit_content, 0);
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");

        InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(edit_content.getWindowToken(), 0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    protected void initView(View root) {
        if(reqBundle != null) {
            requestView = (ReqOverview) root.findViewById(R.id.view_request_trans_main);
            requestView.setLayout(reqBundle);
        }
        edit_content = (EditText) root.findViewById(R.id.edit_trans_main);
        edit_content.requestFocus();
        edit_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                invalidateOptionsMenu();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    protected boolean isReadyToMoveOn() {
        if(edit_content != null) {
            if(edit_content.getText().length() > 0)
                return true;
            return false;
        }
        return false;
    }

    @Override
    public void submitTranslationInfo() {
        if(mListener != null) {
            if (edit_content != null) {
                String text = edit_content.getText().toString();
                mListener.submitTranslationText(text);
            }
        }
    }
}
