package starlord.ciceron.mvp.translate;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.Translation;

/**
 * Created by JaehongPark on 15. 1. 11..
 */
public class TranslateOverview extends RelativeLayout {
    private Bundle transBundle;

    private TextView  text_translatedTime;
    private TextView  text_content;
    private TextView  text_comment;

    public TranslateOverview(Context context) {
        super(context);
        initLayout();
    }

    public TranslateOverview(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout();
    }

    public TranslateOverview(Context context, Bundle transBundle) {
        super(context);
        initLayout();
    }

    public TranslateOverview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLayout();
    }

    public void initLayout() {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_trans_overview, this, true);

        text_translatedTime = (TextView) findViewById(R.id.text_transtime_trans);
        text_content = (TextView) findViewById(R.id.text_text_trans);
        text_comment = (TextView) findViewById(R.id.text_comment_trans);

    }

    public void setLayout(Translation translation) {
        if(translation != null) {
            String transTime = translation.submittedTime;
            String content = translation.content;
            String comment = translation.comment;

            text_translatedTime.setText(transTime);
            text_content.setText(content);
            text_comment.setText(comment);
        }
    }

    public void setLayout(Bundle transBundle) {

        if(transBundle != null) {
            String transTime = transBundle.getString("submittedTime");
            String content = transBundle.getString("content");
            String comment = transBundle.getString("comment");

            text_translatedTime.setText(transTime);
            text_content.setText(content);
            text_comment.setText(comment);
        }
    }
}
