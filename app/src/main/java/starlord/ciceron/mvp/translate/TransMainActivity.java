package starlord.ciceron.mvp.translate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.NewsFeedList;
import starlord.ciceron.mvp.collection.ReqComplete;
import starlord.ciceron.mvp.collection.ReqPending;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.TransComplete;
import starlord.ciceron.mvp.collection.TransPending;
import starlord.ciceron.mvp.collection.Translation;

public class TransMainActivity extends BaseActivity implements BaseTransFragment.OnTranslationListener {
    private FragmentManager fm;
    private FragmentTransaction ft;
    private ProgressBar pb;

    private Bundle      reqBundle;
    private Bundle      transBundle;

    private TransInitFragment       transInitFrag;
    private TransMainFragment       transMainFrag;
    private TransCommentFragment    transCommentFrag;
    private TransReviewFragment     transReviewFrag;

    private static final CharSequence[] stageTitle = {
            "Original Post",
            "Type in Translation",
            "Comment",
            "Review Translation" };

    private static final String    DEBUG_TAG = "[TranslateActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans_main);
        Log.d(DEBUG_TAG, "onCreate");

        initTranslateActivity();

        if(savedInstanceState == null) {
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.frame_translate, transInitFrag);
            ft.commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    /*    @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_request, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();
            switch(id) {
                case android.R.id.home:
                    moveBackToPrevStage();
                    break;

                case R.id.btn_request_nextstep:
                    moveOnToNextStage();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    */



    public void initTranslateActivity() {
        if(getIntent().getExtras() != null)
            reqBundle = getIntent().getExtras();

        fm = getFragmentManager();
        ft = fm.beginTransaction();
        pb = (ProgressBar) findViewById(R.id.pb_translate);

        transInitFrag = TransInitFragment.newInstance(1);
        transMainFrag = TransMainFragment.newInstance(2);
        transCommentFrag = TransCommentFragment.newInstance(3);
        transReviewFrag = TransReviewFragment.newInstance(4);

        ft.replace(R.id.frame_translate, transInitFrag);
        ft.commit();
    }

    @Override
    public void initTranslation() {
        //TODO: modify this
        if(transBundle == null)
            transBundle = new Bundle();

        submitTranslatorId("pjh0308@gmail.com");
        submitTranslatorName("Jaehong Park");
        submitTranslatorPicPath("http://52.11.126.237:5000");
    }

    @Override
    public Bundle getRequestBundle() {
        return reqBundle;
    }

    @Override
    public Bundle getTranslationBundle() {
        return transBundle;
    }


    @Override
    public void onStage(int step) {
        pb.setProgress(step * 25);
        actionBar.setTitle(stageTitle[step-1]);
    }

    @Override
    public void goBackToPreviousStage() {
        Fragment fragment = fm.findFragmentById(R.id.frame_translate);

        if(fragment != null) {
            if (fragment instanceof TransInitFragment) {
                onBackPressed();
                return;
            } else if (fragment instanceof TransMainFragment) {
                fragment = transInitFrag;
            } else if (fragment instanceof TransCommentFragment) {
                fragment = transMainFrag;
            } else if (fragment instanceof TransReviewFragment) {
                fragment = transCommentFrag;
            } else {

            }

            ft = fm.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
            ft.replace(R.id.frame_translate, fragment);
            ft.commit();
        }
    }

    @Override
    public void moveOnToNextStage() {
        Fragment fragment = fm.findFragmentById(R.id.frame_translate);

        if(fragment != null) {

            if (fragment instanceof TransInitFragment) {
                transInitFrag.submitTranslationInfo();
                fragment = transMainFrag;
            } else if (fragment instanceof TransMainFragment) {
                transMainFrag.submitTranslationInfo();
                fragment = transCommentFrag;
            } else if (fragment instanceof TransCommentFragment) {
                transCommentFrag.submitTranslationInfo();
                fragment = transReviewFrag;
            } else if (fragment instanceof TransReviewFragment) {
                // Do nothing
            } else {

            }

            ft = fm.beginTransaction();
            ft.replace(R.id.frame_translate, fragment);
            ft.commit();
        }
    }

    @Override
    public void submitTranslatorId(String value) {
        if(transBundle != null) {
            transBundle.putString("translatorId", value);
        }
    }

    @Override
    public void submitTranslatorName(String value) {
        if(transBundle != null) {
            transBundle.putString("translatorName", value);
        }
    }

    @Override
    public void submitTranslatorPicPath(String value) {
        if(transBundle != null) {
            transBundle.putString("translatorPicPath", value);
        }
    }

    @Override
    public void submitTranslationText(String value) {
        if(transBundle != null) {
            transBundle.putString("content", value);
        }
    }

    @Override
    public void submitTranslationComment(String value) {
        if(transBundle != null) {
            transBundle.putString("comment", value);
        }
    }

    @Override
    public void submitTranslationTime(String value) {
        if(transBundle != null) {
            transBundle.putString("submittedTime", value);
        }
    }

    @Override
    public void submitTranslationTone(int value) {
        if(transBundle != null) {
            transBundle.putInt("tone", value);
        }
    }

    @Override
    public void onComplete() {
        Translation translation = Translation.fromBundle(transBundle);
        if (translation.isValid()) {
            //TODO : MODIFY THIS

            Request request = Request.fromBundle(reqBundle);
            request.status = Request.STATUS_COMPLETED;
            request.setTranslation(translation);

            TransPending.removeRequest(request.id);
            TransComplete.addRequest(request);
            ReqPending.removeRequest(request.id);
            ReqComplete.addRequest(request);
            NewsFeedList.removeRequest(request.id);

            //TODO : MODIFY THIS
        }

        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onBackPressed() {
        showCancelDialog();
    }

    private void showCancelDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle("번역을 취소하실 건가요?")
                .setPositiveButton("예", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    }
                })
                .setNegativeButton("이전으로", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
        builder.create().show();
    }
}
