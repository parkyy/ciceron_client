package starlord.ciceron.mvp.translate;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import starlord.ciceron.mvp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransCommentFragment.OnTranslationListener} interface
 * to handle interaction events.
 * Use the {@link TransCommentFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TransCommentFragment extends BaseTransFragment {

    private EditText edit_comment;
    private static final String DEBUG_TAG = "[TransCommentFragment]";

    public static TransCommentFragment newInstance(int stageNum) {
        TransCommentFragment fragment = new TransCommentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("progressStage", stageNum);
        fragment.setArguments(bundle);

        Log.d(DEBUG_TAG, "newInstance");
        return fragment;
    }

    public TransCommentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (getArguments() != null) {
            progressStage = getArguments().getInt("progressStage");
        }
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_trans_context, container, false);
        initView(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        edit_comment.postDelayed(new Runnable() {
            @Override
            public void run() {
                InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                manager.showSoftInput(edit_comment, 0);
            }
        }, 100);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id) {
            case android.R.id.home:
                if(mListener != null)
                    mListener.goBackToPreviousStage();
                break;

            case R.id.btn_translate_nextstep:
                if(mListener != null)
                    mListener.moveOnToNextStage();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");

        InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(edit_comment.getWindowToken(), 0);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    protected void initView(View root) {
        edit_comment = (EditText) root.findViewById(R.id.edit_trans_comment);
        edit_comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                invalidateOptionsMenu();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edit_comment.requestFocus();
    }

    @Override
    protected boolean isReadyToMoveOn() {
        if(edit_comment != null) {
            if(edit_comment.getText().length() > 0)
                return true;
            return false;
        }
        return false;
    }

    @Override
    public void submitTranslationInfo() {
        if(mListener != null) {
            if(edit_comment != null) {
                String text = edit_comment.getText().toString();
                mListener.submitTranslationComment(text);
                mListener.submitTranslationTime("2015010101");
            }
        }
    }
}
