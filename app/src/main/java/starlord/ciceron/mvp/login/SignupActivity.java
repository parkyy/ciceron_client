package starlord.ciceron.mvp.login;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.HttpURLConnection;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Language;
import starlord.ciceron.mvp.common.Utils;
import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.network.HttpConnManager;
import starlord.ciceron.mvp.network.NetworkAPI;

public class SignupActivity extends BaseActivity {

    private EditText mEmailView;
    private EditText mPasswordView;
    private EditText mUsernameView;
    private Spinner  mLanguageView;
    private Button   btnSignUp;

    private AsyncTask mAuthTask;

    private static final String DEBUG_TAG = "[SignUpActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup2);
        Log.d(DEBUG_TAG, "onCreate");
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mAuthTask != null)
            mAuthTask.cancel(true);
        Log.d(DEBUG_TAG, "onPause");
    }


    private void initView() {
        mEmailView = (EditText) findViewById(R.id.edit_email_signup2);
        mPasswordView = (EditText) findViewById(R.id.edit_pw_signup2);
        mUsernameView = (EditText) findViewById(R.id.edit_username_signup2);
        mLanguageView = (Spinner) findViewById(R.id.spinner_lang_signup2);
        mLanguageView.setAdapter(
                new ArrayAdapter<String>(this,
                                         android.R.layout.simple_spinner_item,
                                         Language.getList()));

        btnSignUp = (Button) findViewById(R.id.btn_signup_signup);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptSignUp();
            }
        });
    }

    private boolean isEmailValid() {
        String email = mEmailView.getText().toString();
        boolean isValid = true;

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            isValid = false;
        } else if (!email.contains("@")) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            isValid = false;
        }

        if (!isValid)
            mEmailView.requestFocus();
        return isValid;
    }

    private boolean isPasswordValid() {
        String password = mPasswordView.getText().toString();
        boolean isValid = true;

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            isValid = false;
        } else if (password.length() < -1) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            isValid = false;
        }
        if (!isValid)
            mPasswordView.requestFocus();
        return isValid;
    }

    private boolean isUsernameValid() {
        String username = mUsernameView.getText().toString();
        boolean isValid = true;

        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            isValid = false;
        }
        if (!isValid)
            mUsernameView.requestFocus();
        return isValid;
    }

    private boolean isLanguageValid() {
        int motherLang = mLanguageView.getSelectedItemPosition();
        boolean isValid = true;

        if (motherLang < 0) {
            isValid = false;
        }
        if (!isValid)
            mLanguageView.requestFocus();
        return isValid;
    }

    private boolean isAccountValid() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);
        mUsernameView.setError(null);

        if (!isEmailValid())
            return false;
        else if (!isPasswordValid())
            return false;
        else if (!isUsernameValid())
            return false;
        else if (!isLanguageValid())
            return false;
        return true;
    }

    public void attemptSignUp() {
        if (isAccountValid()) {
            mAuthTask = new SignUpTask().execute();
        }
    }

    public class SignUpTask extends AsyncTask<Void, Integer, Integer> {
        @Override
        protected void onPreExecute() {
            preExecuteSignUp();
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return executeSignUp();
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
        }
        @Override
        protected void onPostExecute(final Integer statusCode) {
            postExecuteSignUp();
            handleHttpResponse(statusCode);
        }
        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    private int executeSignUp() {
        final String email = mEmailView.getText().toString();
        final String hashedPassword = Utils.hashSHA256(mPasswordView.getText().toString());
        final String username = mUsernameView.getText().toString();
        final String motherLang = String.valueOf(mLanguageView.getSelectedItemPosition());
        int statusCode = -1;

        OkHttpClient client = HttpConnManager.getInstance();

        RequestBody body = new FormEncodingBuilder()
                .add("email", email)
                .add("password", hashedPassword)
                .add("name", username)
                .add("mother_language_id", motherLang)
                .build();

        Request request = new Request.Builder()
                .url(NetworkAPI.SIGNUP_API)
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Accept", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .build();

        try {
            Response response = client.newCall(request).execute();
            statusCode = response.code();
            Log.d(DEBUG_TAG, response.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return statusCode;
    }

    private void preExecuteSignUp() {
        btnSignUp.setEnabled(false);
        hideKeyboard();
        // show progress
    }

    private void postExecuteSignUp() {
        btnSignUp.setEnabled(true);
        // hide progress
    }

    private void handleHttpResponse(int statusCode) {
        switch (statusCode) {
            case HttpURLConnection.HTTP_OK:
                //TODO: Dialog popUp
                setResult(Activity.RESULT_OK);
                finish();
                break;

            // Duplicated ID
            case HttpURLConnection.HTTP_PRECON_FAILED:
                //TODO: Dialog popUp
                break;

            // executeSignUp error / server error
            default:
                //TODO: Dialog popUp
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}
