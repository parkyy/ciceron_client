package starlord.ciceron.mvp.login;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.net.HttpURLConnection;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.main.MainActivity;
import starlord.ciceron.mvp.network.HttpConnManager;
import starlord.ciceron.mvp.network.NetworkAPI;

public class IntroActivity extends Activity {
    private TextView textTitle;
    private Button btnLogin;
    private Button btnSignUp;

    private int LOGIN = 0;
    private int SIGNUP = 1;

    private AsyncTask mAuthTask;
    private static final String DEBUG_TAG = "[IntroActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        initView();
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mAuthTask != null)
            mAuthTask.cancel(true);
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        attemptAutoLogin();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK)
            startMainActivity();
    }

    private void initView() {
        textTitle = (TextView) findViewById(R.id.txt_title_intro);
        textTitle.setTypeface(Fonts.create(getBaseContext()).zeronero());
        btnLogin = (Button) findViewById(R.id.btn_login_intro);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startLoginActivity();
            }
        });

        btnSignUp = (Button) findViewById(R.id.btn_signup_intro);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSignUpActivity();
            }
        });
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, LOGIN);
    }

    private void startSignUpActivity() {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivityForResult(intent, SIGNUP);
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void showLoginUI() {
        btnLogin.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(true);
        btnSignUp.setVisibility(View.VISIBLE);
        btnSignUp.setEnabled(true);
    }

    private void hideLoginUI() {
        btnLogin.setVisibility(View.INVISIBLE);
        btnLogin.setEnabled(false);
        btnSignUp.setVisibility(View.INVISIBLE);
        btnSignUp.setEnabled(false);
    }

    private void attemptAutoLogin() {
        mAuthTask = new AutoLoginTask().execute();
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class AutoLoginTask extends AsyncTask<Void, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            preExecuteAutoLogin();
        }

        @Override
        protected Integer doInBackground(Void... arg) {
            return executeAutoLogin();
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(final Integer statusCode) {
            postExecuteAutoLogin();
            handleHttpResponse(statusCode);
        }
        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    private void preExecuteAutoLogin() {
        hideLoginUI();
    }

    private void postExecuteAutoLogin() {
        showLoginUI();
    }

    private int executeAutoLogin() {
        OkHttpClient client = HttpConnManager.getInstance();
        int statusCode = -1;

        Request request = new Request.Builder()
                .url(NetworkAPI.LOGIN_API)
                .post(null)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Accept", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .build();
        try {
            Response response = client.newCall(request).execute();
            statusCode = response.code();
            Log.d(DEBUG_TAG, "loginResponse:" + response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return statusCode;
    }

    private void handleHttpResponse(int statusCode) {

        switch(statusCode) {
            case HttpURLConnection.HTTP_OK:
                startMainActivity();
                finish();
                break;

            default:
                showLoginUI();
                break;
        }
    }
}
