package starlord.ciceron.mvp.login;

import android.app.Activity;
import android.os.AsyncTask;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Response;
//import com.android.volley.Response;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;

import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.common.Utils;
import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.network.HttpConnManager;
import starlord.ciceron.mvp.network.NetworkAPI;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends BaseActivity {

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;

    private Button btnLogin;
    private Button btnFbLogin;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private AsyncTask mAuthTask = null;

    private static final int SIGNUP = 0;
    private static final int LOGIN = 1;
    private static final String DEBUG_TAG = "[LoginActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);


        Log.d(DEBUG_TAG, "onCreate");
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mAuthTask != null)
            mAuthTask.cancel(true);
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);


    }

    private void initView() {
        TextView title = (TextView) findViewById(R.id.text_title_login2);
        title.setTypeface(Fonts.create(this).zeronero());

        mEmailView = (EditText) findViewById(R.id.edit_id_login2);

        mPasswordView = (EditText) findViewById(R.id.edit_pw_login2);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return true;
            }
        });

        btnLogin = (Button) findViewById(R.id.btn_login_login2);
        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        btnFbLogin = (Button) findViewById(R.id.btn_fblogin_login2);
        btnFbLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private boolean isAccountValid() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        if (!isEmailValid())
            return false;
        return isPasswordValid();
    }

    private boolean isEmailValid() {
        String email = mEmailView.getText().toString();

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            mEmailView.requestFocus();
            return false;
        } else if (!email.contains("@")) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            mEmailView.requestFocus();
            return false;
        }
        return true;
    }

    private boolean isPasswordValid() {
        String password = mPasswordView.getText().toString();

        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            mPasswordView.requestFocus();
            return false;
        } else if (password.length() <= 0) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            mPasswordView.requestFocus();
            return false;
        }
        return true;
    }

    private void attemptLogin() {
        if (isAccountValid()) {
            mAuthTask = new GetLoginSaltTask().execute();
//            mAuthTask = new UserLoginTask().execute();
        }
    }
/*
    private void switchCurrentTabTo(int mode) {
        switch (mode) {
            case SIGNUP:
                enablePasswordEdit(false);
                tab_signup.setSelected(true);
                tab_login.setSelected(false);
                break;

            case LOGIN:
                enablePasswordEdit(true);
                tab_signup.setSelected(false);
                tab_login.setSelected(true);
                break;
        }
    }

    private boolean isSignUpTask() {
        if(tab_signup.isSelected())
            return true;
        return false;
    }

    private void enablePasswordEdit(boolean enabled) {
        mEmailView.setText("");
        mPasswordView.setText("");

        if(enabled) {
            mEmailView.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            mPasswordView.setVisibility(View.VISIBLE);
        }
        else {
            mEmailView.setImeOptions(EditorInfo.IME_ACTION_DONE);
            mPasswordView.setVisibility(View.INVISIBLE);
        }
    }
*/
/*
    public class IdCheckTask extends AsyncTask<Void, Integer, Integer> {

        private final String mEmail;

        IdCheckTask(String email) {
            this.mEmail = email;
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return executeIdCheck(mEmail);
        }

        @Override
        protected void onPostExecute(final Integer statusCode) {

            // TODO : MODIFY THIS
            if (statusCode != HttpURLConnection.HTTP_OK)
                startSignUpActivity();
            else
                Toast.makeText(getBaseContext(), "Duplicated Id", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }
*/
    public class GetLoginSaltTask extends AsyncTask<Void, Integer, String> {

        @Override
        protected void onPreExecute() {
            preExecuteLogin();
        }

        @Override
        protected String doInBackground(Void... params) {
            return executeGetSalt();
        }
        @Override
        protected void onPostExecute(final String salt) {
            if (salt == null) {
                postExecuteLogin();
                return;
            }
            mAuthTask = new UserLoginTask().execute(salt);
        }
        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<String, Integer, Integer> {

        @Override
        protected void onPreExecute() {
            preExecuteLogin();
        }

        @Override
        protected Integer doInBackground(String... loginSalt) {
            return executeLogin(loginSalt[0]);
            //return executeLogin(loginSalt[0]);
        }
        @Override
        protected void onProgressUpdate(Integer... values) {
        }

        @Override
        protected void onPostExecute(final Integer statusCode) {
            postExecuteLogin();
            handleHttpResponse(statusCode);
        }
        @Override
        protected void onCancelled() {
            mAuthTask = null;
        }
    }

    private String executeGetSalt() {
        OkHttpClient client = HttpConnManager.getInstance();
        String loginSalt = null;

        Request request = new Request.Builder()
                .url(NetworkAPI.LOGIN_API).get().build();
        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                loginSalt = new JSONObject(response.body().string()).getString("identifier");
            }
            Log.d(DEBUG_TAG, "loginSalt: " + loginSalt);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return loginSalt;
    }

    private int executeLogin(final String loginSalt) {
        final String email = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();
        final String hashedPassword =
                Utils.hashSHA256(loginSalt + Utils.hashSHA256(password) + loginSalt);
        Log.d(DEBUG_TAG, hashedPassword);
        OkHttpClient client = HttpConnManager.getInstance();
        int statusCode = -1;

        RequestBody body = new FormEncodingBuilder()
                .add("email", email)
                .add("password", hashedPassword)
                .build();

        Request request = new Request.Builder()
                .url(NetworkAPI.LOGIN_API)
                .post(body)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Accept", "application/json")
                .addHeader("Cache-Control", "no-cache")
                .build();
        try {
            Response response = client.newCall(request).execute();

            statusCode = response.code();
            Log.d(DEBUG_TAG, "loginResponse:" + response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return statusCode;
    }

    public int executeLogin2(final String loginSalt) {
        final String email = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();
//        final String hashedPassword =
//                Utils.hashSHA256(loginSalt + Utils.hashSHA256(password) + loginSalt);

        int statusCode = -1;
        HttpClient httpClient = new DefaultHttpClient();
        String urlString = NetworkAPI.LOGIN_API;
        try {
            URI url = new URI(urlString);

            HttpPost httpPost = new HttpPost(urlString);
            //httpPost.setURI(url);


            List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("email", email));
            nameValuePairs.add(new BasicNameValuePair("password", password));

            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = httpClient.execute(httpPost);
            statusCode = response.getStatusLine().getStatusCode();

            String responseString = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            Log.d(DEBUG_TAG, "statusCode: " + String.valueOf(statusCode));
            Log.d(DEBUG_TAG, "loginResponse: " + responseString);

        } catch (URISyntaxException e) {
            Log.e(DEBUG_TAG, e.getLocalizedMessage());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.e(DEBUG_TAG, e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e(DEBUG_TAG, e.getLocalizedMessage());
            e.printStackTrace();
        }
        return statusCode;
    }

    private void preExecuteLogin() {
        btnLogin.setEnabled(false);
        hideKeyboard();
        // show progress
    }

    private void postExecuteLogin() {
        btnLogin.setEnabled(true);
        // hide progress
    }

    private void handleHttpResponse(int statusCode) {

        switch(statusCode) {
            case HttpURLConnection.HTTP_OK:
                setResult(Activity.RESULT_OK);
                finish();
                break;

            // inValid email or wrong password
            case HttpURLConnection.HTTP_FORBIDDEN:
                //TODO: Dialog popUp
                Toast.makeText(this, "forbidden", Toast.LENGTH_SHORT).show();
                //setResult(Activity.RESULT_OK);
                //finish();
                break;

            // Found more than 2 records of same user ID
            default:
                //TODO: Dialog popUp
                Toast.makeText(this, "same id", Toast.LENGTH_SHORT).show();
                //setResult(Activity.RESULT_OK);
                //finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}



