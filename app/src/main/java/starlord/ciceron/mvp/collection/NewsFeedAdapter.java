package starlord.ciceron.mvp.collection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.common.Format;
import starlord.ciceron.mvp.common.Language;
import starlord.ciceron.mvp.common.Subject;

/**
 * Created by JaehongPark on 14. 12. 28..
 */
public class NewsFeedAdapter extends ArrayAdapter<Request> {
    private Context context;
    private ArrayList<Request> list;

    public NewsFeedAdapter(Context context, ArrayList<Request> list) {
        super(context, R.layout.item_list_newsfeed3, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public void addAll(Collection<? extends Request> collection) {
        super.addAll(collection);
    }

    @Override
    public Request getItem(int position) {
        return super.getItem(position);
    }

    protected class ViewHolder {
        ImageView picture;
        TextView  name;

        ImageView img_format;
        ImageView img_subject;
        TextView  text_format;
        TextView  text_subject;

        ImageView img_originalLang;
        ImageView img_targetLang;
        TextView  text_originalLang;
        TextView  text_targetLang;

        TextView  dueTime;
        TextView  words;
        TextView  words2;
        TextView  price;

    }

    public View getView(int index, View view, ViewGroup parent) {
        final ViewHolder holder;
        Request request = list.get(index);

        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_list_newsfeed3, null, false);
            holder = new ViewHolder();
            holder.picture = (ImageView) view.findViewById(R.id.img_reqItem_profile);
            holder.name = (TextView) view.findViewById(R.id.text_reqItem_clientName);

            holder.img_format = (ImageView) view.findViewById(R.id.img_reqItem_format);
            holder.img_subject = (ImageView) view.findViewById(R.id.img_reqItem_subject);
            holder.text_format = (TextView) view.findViewById(R.id.text_reqItem_format);
            holder.text_subject = (TextView) view.findViewById(R.id.text_reqItem_subject);

            holder.img_originalLang = (ImageView) view.findViewById(R.id.img_reqItem_lang_origin);
            holder.img_targetLang = (ImageView) view.findViewById(R.id.img_reqItem_lang_target);
            holder.text_originalLang = (TextView) view.findViewById(R.id.text_reqItem_lang_origin);
            holder.text_targetLang = (TextView) view.findViewById(R.id.text_reqItem_lang_target);

            holder.dueTime = (TextView) view.findViewById(R.id.text_reqItem_dueTime);
            holder.words = (TextView) view.findViewById(R.id.text_reqItem_words);
            //holder.words2 = (TextView) view.findViewById(R.id.text_words2_news);
            holder.price = (TextView) view.findViewById(R.id.text_reqItem_price);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        int format = request.format;
        int subject = request.subject;
        int originalLang = request.originalLang;
        int targetLang = request.targetLang;
        StringBuffer words = new StringBuffer(String.valueOf(request.words));
        StringBuffer price = new StringBuffer(String.valueOf(request.points));

        holder.name.setText(request.clientName);
        holder.name.setTypeface(Fonts.create(context).appleGothic());

        holder.img_format.setImageResource(Format.getDrawable(format));
        holder.img_subject.setImageResource(Subject.getDrawable(subject));
        holder.text_format.setText(Format.getTitle(format));
        holder.text_format.setTypeface(Fonts.create(context).appleGothic());
        holder.text_subject.setText(Subject.getTitle(subject));
        holder.text_subject.setTypeface(Fonts.create(context).appleGothic());

        holder.img_originalLang.setImageResource(Language.getDrawable(originalLang));
        holder.img_targetLang.setImageResource(Language.getDrawable(targetLang));
        holder.text_originalLang.setText(Language.getTitle(originalLang));
        holder.text_originalLang.setTypeface(Fonts.create(context).appleGothic());
        holder.text_targetLang.setText(Language.getTitle(targetLang));
        holder.text_targetLang.setTypeface(Fonts.create(context).appleGothic());

        holder.dueTime.setText(request.dueTime);
        holder.dueTime.setTypeface(Fonts.create(context).appleGothic());
//        holder.words2.setText(String.valueOf(words.toString()));
//        holder.words2.setTypeface(Fonts.create(context).gotham());
        holder.words.setText(words.append(" 단어").toString());
        holder.words.setTypeface(Fonts.create(context).appleGothic());
//        holder.price.setText(String.valueOf(request.points));
        holder.price.setText(price.append(" $").toString());
        holder.price.setTypeface(Fonts.create(context).appleGothic());

        return view;
    }
}
