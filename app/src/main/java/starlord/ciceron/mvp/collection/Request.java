package starlord.ciceron.mvp.collection;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Date;


/**
 * Created by JaehongPark on 14. 11. 6..
 */
public class Request {

    public static final int STATUS_PENDING = 0;
    public static final int STATUS_ONGOING = 1;
    public static final int STATUS_COMPLETED = 2;

    public static final int TYPE_TEXT = 100;
    public static final int TYPE_IMAGE = 101;
    public static final int TYPE_SOUND = 102;
    public static final int TYPE_FILE = 103;

    // Common required field //
    public int      id;
    public String   clientId;
    public String   clientName;
    public String   clientPicPath;
    public int      originalLang;
    public int      targetLang;
    public boolean  isSos;
    public int      format;
    public int      subject;
    public int      type;
    public String   content;
    public String   context;
    public int      status;
    public Date     registeredTime;
    public Date     dueTime;
    public Date     expectedTime;
    public int      words;
    public int      points;
    public String   title;

    // Additional field //
    public ArrayList<Profile> translatorsInQueue;
    public Translation translation;

    public Request() {
        id = -1;
        clientId = null;
        clientName = null;
        clientPicPath = null;
        originalLang = -1;
        targetLang = -1;
        isSos = false;
        format = -1;
        subject = -1;
        type = -1;
        content = null;
        context = null;
        status = Request.STATUS_PENDING;
        registeredTime = null;
        dueTime = null;
        expectedTime = null;
        words = 0;
        points = 0;
        title = null;

        translatorsInQueue = null;
        translation = null;
    }

    public Request(final int id, final String clientId, final String clientName,
                   final String clientPicPath,final int originalLang, final int targetLang,
                   final boolean isSos, final int format, final int subject,
                   final int type, final String content, final String context,
                   final int status, final Date registeredTime, final Date dueTime,
                   final Date expectedTime, final int words, final int points, final String title) {

        this.id = id;
        this.clientId = clientId;
        this.clientName = clientName;
        this.clientPicPath = clientPicPath;
        this.originalLang = originalLang;
        this.targetLang = targetLang;
        this.isSos = isSos;
        this.format = format;
        this.subject = subject;
        this.type = type;
        this.content = content;
        this.context = context;
        this.status  = status;
        this.registeredTime = registeredTime;
        this.dueTime = dueTime;
        this.expectedTime = expectedTime;
        this.words = words;
        this.points = points;
        this.title = title;
    }


    public Bundle toBundle() {
        if(this.isValid()) {
            Bundle bundle = new Bundle();
            bundle.putInt("id", id);
            bundle.putString("clientId", clientId);
            bundle.putString("clientName", clientName);
            bundle.putString("clientPicPath", clientPicPath);
            bundle.putInt("originalLang", originalLang);
            bundle.putInt("targetLang", targetLang);
            bundle.putBoolean("isSos", isSos);
            bundle.putInt("format", format);
            bundle.putInt("subject", subject);
            bundle.putInt("type", type);
            bundle.putString("content", content);
            bundle.putString("context", context);
            bundle.putInt("status", status);
            bundle.putSerializable("registeredTime", registeredTime);
            bundle.putSerializable("dueTime", dueTime);
            bundle.putSerializable("expectedTime", expectedTime);
            bundle.putInt("words", words);
            bundle.putInt("points", points);
            bundle.putString("title", title);
            bundle.putParcelableArrayList("translatorsInQueue", translatorsInQueue);

            if(translation != null)
                bundle.putBundle("transBundle", translation.toBundle());

            return bundle;
        }
        return null;
    }

    public static Request fromBundle(Bundle bundle) {
        int  id                 = bundle.getInt("id");
        String  clientId        = bundle.getString("clientId");
        String  clientName      = bundle.getString("clientName");
        String  clientPicPath   = bundle.getString("clientPicPath");
        int     originalLang    = bundle.getInt("originalLang");
        int     targetLang      = bundle.getInt("targetLang");
        boolean isSos           = bundle.getBoolean("isSos");
        int     format          = bundle.getInt("format");
        int     subject         = bundle.getInt("subject");
        int     type            = bundle.getInt("type");
        String  content         = bundle.getString("content");
        String  context         = bundle.getString("context");
        int     status          = bundle.getInt("status");
        Date    registeredTime  = (Date) bundle.getSerializable("registeredTime");
        Date    dueTime         = (Date) bundle.getSerializable("dueTime");
        Date    expectedTime    = (Date) bundle.getSerializable("expectedTime");
        int     words           = bundle.getInt("words");
        int     points          = bundle.getInt("points");
        String  title           = bundle.getString("title");

        ArrayList<Profile> translatorsInQueue = bundle.getParcelableArrayList("translatorsInQueue");
        Bundle transBundle      = bundle.getBundle("transBundle");

        Request request = new Request(id, clientId, clientName, clientPicPath,
                originalLang, targetLang, isSos,
                format, subject, type, content, context,
                status, registeredTime, dueTime,
                expectedTime, words, points, title);

        request.translatorsInQueue = translatorsInQueue;
        request.translation = Translation.fromBundle(transBundle);

        return request;
    }

    public boolean isValid() {
        if(clientId == null)
            return false;
        if(content == null)
            return false;
        else if(type < Request.TYPE_TEXT || type > Request.TYPE_FILE)
            return false;
        else if(originalLang < 0 || targetLang < 0)
            return false;
        else if(registeredTime == null || dueTime == null)
            return false;
        else if(status < Request.STATUS_PENDING || status > Request.STATUS_COMPLETED)
            return false;
        return true;
    }

    public boolean isTextType() {
        return (type == TYPE_TEXT);
    }

    public boolean isImageType() {
        return (type == TYPE_IMAGE);
    }

    public boolean isSoundType() {
        return (type == TYPE_SOUND);
    }

    public boolean isFileType() {
        return (type == TYPE_FILE);
    }


    public boolean isCompleted() {
        if(status == Request.STATUS_COMPLETED)
            if(translation != null)
                return true;
        return false;
    }

}
