package starlord.ciceron.mvp.collection;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;

import starlord.ciceron.mvp.R;

/**
 * Created by JaehongPark on 15. 1. 24..
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter implements ActionBar.TabListener,
        ViewPager.OnPageChangeListener {
    private final Context mContext;
    private final ActionBar mActionBar;
    private final ViewPager mViewPager;
    private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

    private final int[] ICONS = new int[] {
            R.drawable.ic_tab_newsfeed,
            R.drawable.ic_tab_request,
            R.drawable.ic_tab_translate,
            R.drawable.ic_tab_translate,
            R.drawable.ic_tab_alert,
            R.drawable.ic_tab_setting
    };

    private final int[] SELECTED_ICONS = new int[] {
            R.drawable.ic_tab_newsfeed_selected,
            R.drawable.ic_tab_request_selected,
            R.drawable.ic_tab_translate_selected,
            R.drawable.ic_tab_translate_selected,
            R.drawable.ic_tab_alert_selected,
            R.drawable.ic_tab_setting_selected
    };


    static final class TabInfo {
        private final Class<?> clss;
        private final Bundle args;

        TabInfo(Class<?> _class, Bundle _args) {
            clss = _class;
            args = _args;
        }
    }

    public SectionsPagerAdapter(Activity activity, ViewPager pager) {
        super(activity.getFragmentManager());
        mContext = activity;
        mActionBar = activity.getActionBar();
        mViewPager = pager;
        mViewPager.setAdapter(this);
        mViewPager.setOnPageChangeListener(this);
    }

    public void addTab(ActionBar.Tab tab, Class<?> clss, Bundle args) {
        TabInfo info = new TabInfo(clss, args);
        tab.setTag(info);

        tab.setTabListener(this);
        mTabs.add(info);
        mActionBar.addTab(tab);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public Fragment getItem(int position) {
        TabInfo info = mTabs.get(position);
        return Fragment.instantiate(mContext, info.clss.getName(), info.args);
    }

    /** OnPageChangeListener **/
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        // When swiping between pages, select the
        // corresponding tab.
        mActionBar.setSelectedNavigationItem(position);

    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    /** TabListener **/
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition(), true);
        mActionBar.setTitle(tab.getContentDescription().toString());
//        tab.setIcon(SELECTED_ICONS[tab.getPosition()]);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
//        tab.setIcon(ICONS[tab.getPosition()]);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
