package starlord.ciceron.mvp.collection;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by JaehongPark on 15. 4. 12..
 */
public class Profile implements Parcelable {

    private String  userId;
    private String  userName;
    private String  userPicPath;
    private int     userCategory;
    private int     userMotherLang;
    private int[]   userTranslatableLang;
    private int[]   userBadgeList;

    private int     numOfPendingRequests;
    private int     numOfOngoingRequests;
    private int     numOfCompleteRequests;

    private int     numOfPendingTranslations;
    private int     numOfOngoingTranslations;
    private int     numOfCompleteTranslations;

    private double  userRevenue;
    private String  userProfileText;

    private static int CLIENT = 0;
    private static int TRANSLATOR = 1;

    public Profile() {
        userId = null;
        userName = null;
        userPicPath = null;
        userCategory = CLIENT;
        userMotherLang = -1;
        userTranslatableLang = null;
        userBadgeList = null;

        numOfPendingRequests = -1;
        numOfOngoingRequests = -1;
        numOfCompleteRequests = -1;

        numOfPendingTranslations = -1;
        numOfOngoingTranslations = -1;
        numOfCompleteTranslations = -1;

        userRevenue = -1;
        userProfileText = null;
    }

    public Profile(Parcel in) {
        readFromParcel(in);
    }

    public Profile(String id, String name, String picPath) {
        this.userId = id;
        this.userName = name;
        this.userPicPath = picPath;
    }

    public Profile(String id, String name, String picPath, int[] badgeList) {
        this.userId = id;
        this.userName = name;
        this.userPicPath = picPath;
        this.userBadgeList = badgeList;
    }

    public Profile(String id, String name, String picPath, int category,
                   int motherLang, int[] translatableLang, int[] badgeList,
                   int pendingReq, int ongoingReq, int completeReq,
                   int pendingTrans, int ongoingTrans, int completeTrans,
                   double revenue, String profileText) {
        userId = id;
        userName = name;
        userPicPath = picPath;
        userCategory = category;
        userMotherLang = motherLang;
        userTranslatableLang = translatableLang;
        userBadgeList = badgeList;
        numOfPendingRequests = pendingReq;
        numOfOngoingRequests = ongoingReq;
        numOfCompleteRequests = completeReq;
        numOfPendingTranslations = pendingTrans;
        numOfOngoingTranslations = ongoingTrans;
        numOfCompleteTranslations = completeTrans;
        userRevenue = revenue;
        userProfileText = profileText;
    }

    public static final Parcelable.Creator<Profile> CREATOR
            = new Parcelable.Creator<Profile>() {
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(userId);
        parcel.writeString(userName);
        parcel.writeString(userPicPath);
        parcel.writeInt(userCategory);
        parcel.writeInt(userMotherLang);
        parcel.writeIntArray(userTranslatableLang);
        parcel.writeIntArray(userBadgeList);

        parcel.writeInt(numOfPendingRequests);
        parcel.writeInt(numOfOngoingRequests);
        parcel.writeInt(numOfCompleteRequests);

        parcel.writeInt(numOfPendingTranslations);
        parcel.writeInt(numOfOngoingTranslations);
        parcel.writeInt(numOfCompleteTranslations);

        parcel.writeDouble(userRevenue);
        parcel.writeString(userProfileText);

        parcel.writeInt(CLIENT);
        parcel.writeInt(TRANSLATOR);
    }

    public void readFromParcel(Parcel parcel) {
        userId = parcel.readString();
        userName = parcel.readString();
        userPicPath = parcel.readString();
        userCategory = parcel.readInt();
        userMotherLang = parcel.readInt();
        userTranslatableLang = parcel.createIntArray();
        userBadgeList = parcel.createIntArray();

        numOfPendingRequests = parcel.readInt();
        numOfOngoingRequests = parcel.readInt();
        numOfCompleteRequests = parcel.readInt();

        numOfPendingTranslations = parcel.readInt();
        numOfOngoingTranslations = parcel.readInt();
        numOfCompleteTranslations = parcel.readInt();

        userRevenue = parcel.readDouble();
        userProfileText = parcel.readString();

        CLIENT = parcel.readInt();
        TRANSLATOR = parcel.readInt();
    }
}
