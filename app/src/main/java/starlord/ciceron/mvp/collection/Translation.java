package starlord.ciceron.mvp.collection;

import android.os.Bundle;

import java.util.Date;

/**
 * Created by JaehongPark on 15. 1. 10..
 */
public class Translation {
    public Profile translatorProfile;
    public String content;
    public String comment;
    public Date   submittedTime;
    public int    tone;
    public int    feedbackScore;

    public Translation() {
        translatorProfile = null;
        content = null;
        comment = null;
        submittedTime = null;
        tone = -1;
        feedbackScore = -1;
    }

    public Translation(Profile profile, final String content,
                       final String comment, final Date submittedTime,
                       final int tone, final int feedbackScore) {

        this.translatorProfile = profile;
        this.content = content;
        this.comment = comment;
        this.submittedTime = submittedTime;
        this.tone = tone;
        this.feedbackScore = feedbackScore;
    }

    public boolean isValid() {
        if(translatorProfile == null)
            return false;
        else if(content == null)
            return false;
        else if(comment == null)
            return false;
        else if(submittedTime == null)
            return false;
        else if(tone < 0)
            return false;
        return true;
    }

    public Bundle toBundle() {
        if(this.isValid()) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("translatorProfile", translatorProfile);
            bundle.putString("content", content);
            bundle.putString("comment", comment);
            bundle.putSerializable("submittedTime", submittedTime);
            bundle.putInt("tone", tone);
            bundle.putInt("feedbackScore", feedbackScore);
            return bundle;
        }
        return null;
    }

    public static Translation fromBundle(Bundle bundle) {
        if(bundle == null)
            return null;

        Profile translatorProfile  = bundle.getParcelable("translatorProfile");
        String translationContent     = bundle.getString("content");
        String translationComment  = bundle.getString("comment");
        Date   translationTime     = (Date) bundle.getSerializable("submittedTime");
        int    translationTone     = bundle.getInt("tone");
        int    translationFeedbackScore = bundle.getInt("feedbackScore");

        Translation translation = new Translation(translatorProfile, translationContent,
                                        translationComment, translationTime,
                                        translationTone, translationFeedbackScore);
        return translation;
    }
}
