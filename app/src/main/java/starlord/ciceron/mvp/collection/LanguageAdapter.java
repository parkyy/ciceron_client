package starlord.ciceron.mvp.collection;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.common.Language;

/**
 * Created by JaehongPark on 15. 1. 12..
 */
public class LanguageAdapter extends ArrayAdapter<String> {
    private Activity activity;

    public LanguageAdapter(Activity activity) {
        super(activity, R.layout.item_list_lang, Language.getList());
        this.activity = activity;
    }

    private class ViewHolder {
        TextView lang_txt;
        ImageView lang_img;
    }

    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;

        if (view == null) {
            view = LayoutInflater.from(activity).inflate(R.layout.item_list_lang, null, false);
            holder = new ViewHolder();
            holder.lang_txt = (TextView) view.findViewById(R.id.text_req_lang);
            holder.lang_img = (ImageView) view.findViewById(R.id.img_req_lang);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.lang_txt.setText(Language.getTitle(position));
        holder.lang_img.setImageResource(Language.getDrawable(position));

        return view;
    }
}
