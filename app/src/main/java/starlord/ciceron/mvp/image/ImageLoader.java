package starlord.ciceron.mvp.image;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

import starlord.ciceron.mvp.BuildConfig;

/**
 * Created by JaehongPark on 15. 2. 8..
 */
public class ImageLoader {
    private ImageCache imageCache;
    private Resources mResources;

    private static final int MESSAGE_CLEAR = 0;
    private static final int MESSAGE_INIT_DISK_CACHE = 1;
    private static final int MESSAGE_FLUSH = 2;
    private static final int MESSAGE_CLOSE = 3;

    private static ImageLoader imageLoader;
    private static final String TAG = "ImageLoader";

    private ImageLoader() {

    }

    /* Return singleton imageloader class instance */
    public static ImageLoader getInstance() {
        if(imageLoader == null) {
            synchronized (ImageLoader.class) {
                if (imageLoader == null) {
                    imageLoader = new ImageLoader();
                }
            }
        }
        return imageLoader;
    }

    public synchronized void init(Resources resources,
                                  ImageCache.ImageCacheParams cacheParams) {
        this.mResources = resources;
        imageCache = ImageCache.getInstance(cacheParams);
    }

    public ImageCache getImageCache() {
        return imageCache;
    }


    public void loadImage(String imageKey, ImageView imageView) {
        Log.d(TAG, "Loading image:" + imageKey);

        if(imageKey == null || imageView == null)
            return;

        final Bitmap bitmap = imageCache.getBitmapFromMemCache(imageKey);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            if (cancelPotentialWork(imageKey, imageView)) {
                final ImageWorkerTask task = new ImageWorkerTask(imageView);
                final AsyncDrawable asyncDrawable =
                        new AsyncDrawable(mResources, null, task);
                imageView.setImageDrawable(asyncDrawable);
                task.execute(imageKey);
            }
        }
    }

    public void close() {
        if(imageCache != null)
            imageCache.close();
    }

    /**
     * Cancels any pending work attached to the provided ImageView.
     * @param imageView
     */
    private void cancelWork(ImageView imageView) {
        final ImageWorkerTask imageWorkerTask = getBitmapWorkerTask(imageView);
        if (imageWorkerTask != null) {
            imageWorkerTask.cancel(true);
            if (BuildConfig.DEBUG) {
                final Object bitmapData = imageWorkerTask.imageKey;
                Log.d(TAG, "cancelWork - cancelled work for " + bitmapData);
            }
        }
    }

    /**
     * Returns true if the current work has been canceled or if there was no work in
     * progress on this image view.
     * Returns false if the work in progress deals with the same data. The work is not
     * stopped in that case.
     */
    private boolean cancelPotentialWork(String key, ImageView imageView) {
        final ImageWorkerTask imageWorkerTask = getBitmapWorkerTask(imageView);

        if (imageWorkerTask != null) {
            final String bitmapKey = imageWorkerTask.imageKey;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapKey == null || !bitmapKey.equals(key)) {
                // Cancel previous task
                imageWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    private ImageWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }

    /**
     * The actual AsyncTask that will asynchronously process the image.
     */
    public class ImageWorkerTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;
        private static final String TAG = "ImageWorkerTask";
        String imageKey = null;

        public ImageWorkerTask(ImageView imageView) {
            // Use a WeakReference to ensure the ImageView can be garbage collected
            imageViewReference = new WeakReference<ImageView>(imageView);
        }

        /*
         * Decode image in background.
         */
        @Override
        protected Bitmap doInBackground(String... params) {
            if(BuildConfig.DEBUG) {
                Log.d(TAG, "doInBackground");
            }

            imageKey = params[0];
            Bitmap bitmap = null;

            // Check cache in background thread
            if(imageCache != null && !isCancelled())
                bitmap = imageCache.getBitmapFromDiskCache(imageKey, imageViewReference.get());
            // Not found in disk cache
            if (bitmap == null && !isCancelled()) {
                // Process as normal
                bitmap = ImageWorker.loadBitmapFromFile(imageKey, imageViewReference.get(), imageCache);
            }

            // If the bitmap was processed and the image cache is available, then add the processed
            // bitmap to the cache for future use. Note we don't check if the task was cancelled
            // here, if it was, and the thread is still running, we may as well add the processed
            // bitmap to our cache as it might be used again in the future
            // Add final bitmap to caches
            if (bitmap != null) {
                if (imageCache != null)
                    imageCache.addBitmapToCache(imageKey, bitmap);
            }

            return bitmap;
        }

        // Once complete, see if ImageView is still around and set bitmap.
        @Override
        protected void onPostExecute(Bitmap bitmap) {

            // if cancel was called on this task then we're done
            if (isCancelled()) {
                bitmap = null;
            }

            if (bitmap != null) {
                final ImageView imageView = imageViewReference.get();
                final ImageWorkerTask imageWorkerTask =
                        getBitmapWorkerTask(imageView);

                if (this == imageWorkerTask && imageView != null) {
                    if(BuildConfig.DEBUG) {
                        Log.d(TAG, "onPostExecute - setting bitmap");
                    }
                    imageView.setImageBitmap(bitmap);
                }
            }
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);

        }
    }


}
