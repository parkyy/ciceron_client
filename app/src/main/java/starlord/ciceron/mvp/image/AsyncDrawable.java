package starlord.ciceron.mvp.image;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

import java.lang.ref.WeakReference;


import starlord.ciceron.mvp.image.ImageLoader;
import starlord.ciceron.mvp.image.ImageLoader.ImageWorkerTask;
/**
 * Created by JaehongPark on 15. 2. 8..
 */

public class AsyncDrawable extends BitmapDrawable {
    private final WeakReference<ImageWorkerTask> bitmapWorkerTaskReference;

    public AsyncDrawable(Resources res, Bitmap bitmap,
                         ImageWorkerTask imageWorkerTask) {
        super(res, bitmap);
        bitmapWorkerTaskReference = new WeakReference<ImageWorkerTask>(imageWorkerTask);
    }

    public ImageWorkerTask getBitmapWorkerTask() {
        return bitmapWorkerTaskReference.get();
    }
}
