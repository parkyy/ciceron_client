package starlord.ciceron.mvp.network;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by JaehongPark on 15. 4. 20..
 */
public class ServerStatusRequest extends Request<Integer> {
    private Response.Listener<Integer> mListener;
    private Response.ErrorListener mErrorListener;
    private String  mContentType;


    public ServerStatusRequest (int method,
                                String url,
                                Response.Listener<Integer> listener,
                                Response.ErrorListener errorListener) {

        super(method, url, errorListener);
        mListener = listener;
        mErrorListener = errorListener;

        if (method == Method.POST) {
            // DefaultRetryPolicy(initialTimeOut, maxNumReTries, backoffMultiplier)
            RetryPolicy policy = new DefaultRetryPolicy(5000, 0, 5);
            setRetryPolicy(policy);
        }

    }

    public ServerStatusRequest(String url,
                               Response.Listener<Integer> listener,
                               Response.ErrorListener errorListener) {

        super(Method.GET, url, errorListener);
        mListener = listener;
        mErrorListener = errorListener;
    }

    @Override
    protected Response<Integer> parseNetworkResponse(NetworkResponse response) {
        return Response.success (response.statusCode, HttpHeaderParser.parseCacheHeaders(response));
    }


    @Override
    protected void deliverResponse(Integer statusCode) {
        if (mListener != null)
            mListener.onResponse(statusCode);
    }

    @Override
    public void deliverError(VolleyError error) {
        if (mErrorListener != null)
            mErrorListener.onErrorResponse(error);
    }

    @Override
    public String getBodyContentType() {
        if (mContentType != null)
            return mContentType;
        return super.getBodyContentType();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        return super.getBody();
    }

    public void setContentType(String mContentType) {
        this.mContentType = mContentType;
    }
}
