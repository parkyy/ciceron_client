package starlord.ciceron.mvp.network;

/**
 * Created by JaehongPark on 15. 7. 2..
 */
public class NetworkConfig {
    public static final int MAX_TOTAL_CONNECTION = 20;
    public static final int MAX_CONNECTIONS_PER_ROUTE = 5;
    public static final int SOCKET_TIMEOUT = 10000;
    public static final long CONNECTION_TIMEOUT = 10000;
    public static final long UPDATE_PERIOD = 300000;
}
