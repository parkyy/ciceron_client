package starlord.ciceron.mvp.network;

public class NetworkAPI {

	public static final String HOST_PROTOCOL = "http://";
	public static final String HOST_URL = "52.11.126.237";
	public static final int PORT_NUMBER = 5000;
	public static final int PORT_NUMBER_HTTPS = 443;
	
	public static final String API_URL = HOST_PROTOCOL + HOST_URL + ":" + PORT_NUMBER + "/api";
	public static final String LOGIN_API = API_URL + "/login";
    public static final String IDCHECK_API = API_URL + "/idCheck";
	public static final String SIGNUP_API = API_URL + "/signup";

    public static final String NEWSFEED_API = API_URL + "/requests";

    public static final String USER_REQUEST_API = API_URL + "/user/requests";
    public static final String USER_REQUEST_PENDING_URL = USER_REQUEST_API + "/pending";
    public static final String USER_REQUEST_ONGOING_URL = USER_REQUEST_API + "/ongoing";
    public static final String USER_REQUEST_COMPLETE_URL = USER_REQUEST_API + "/complete";

    public static final String USER_TRANSLATION_API = API_URL + "/user/translations";
    public static final String USER_TRANSLATION_PENDING_API = USER_TRANSLATION_API + "/pending";
    public static final String USER_TRANSLATION_ONGOING_API = USER_TRANSLATION_API + "/ongoing";
    public static final String USER_TRANSLATION_COMPLETE_URL = USER_TRANSLATION_API + "/complete";

    public static final String USER_PROFILE_URL = API_URL + "/user/profile";


}