package starlord.ciceron.mvp.network;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import starlord.ciceron.mvp.collection.Profile;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.Translation;

/**
 * Created by JaehongPark on 15. 4. 8..
 */
public class ResponseJSONParser {
    private static final String DEBUG_TAG = "ResponseJSONParser";

    public static ArrayList<Request> parseRequestList(JSONArray jsonArray) {
        ArrayList<Request> requests = new ArrayList<Request>();
        int size = jsonArray.length();
        try {
            for (int i = 0; i < size; i++) {
                requests.add(parseRequest(jsonArray.getJSONObject(i)));
            }
        } catch (JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
            e.printStackTrace();
        }
        return requests;
    }

    public static Request parseRequest(JSONObject jsonObject) {
        Request request = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            int  id       = jsonObject.getInt("request_id");
            String  clientId = jsonObject.getString("request_clientId");
            String  clientName = jsonObject.getString("request_clientName");
            String  clientPicPath = jsonObject.getString("request_clientPicPath");
            int     originalLang = jsonObject.getInt("request_originalLang");
            int     targetLang = jsonObject.getInt("request_targetLang");
            boolean isSos = jsonObject.getBoolean("request_isSos");
            int     format = jsonObject.getInt("request_format");
            int     subject = jsonObject.getInt("request_subject");

            int type = -1;
            String content = null;
            if (jsonObject.getBoolean("request_isText")) {
                type = Request.TYPE_TEXT;
                content = jsonObject.getString("request_text");
            }
            else if (jsonObject.getBoolean("request_isPhoto")) {
                type = Request.TYPE_IMAGE;
                content = jsonObject.getString("request_photoPath");
            }
            else if (jsonObject.getBoolean("request_isSound")) {
                type = Request.TYPE_SOUND;
                content = jsonObject.getString("request_soundPath");
            }
            else if (jsonObject.getBoolean("request_isFile")) {
                type = Request.TYPE_FILE;
                content = jsonObject.getString("request_filePath");
            }
            String context = jsonObject.getString("request_context");
            int status = jsonObject.getInt("request_status");

            Date registeredTime = dateFormat.parse(jsonObject.getString("request_registeredTime"));
            Date dueTime = dateFormat.parse(jsonObject.getString("request_dueTime"));
            Date expectedTime = dateFormat.parse(jsonObject.getString("request_expectedTime"));
            int    words = jsonObject.getInt("request_words");
            int    points = jsonObject.getInt("request_points");
            String    title = jsonObject.getString("request_title");

            request = new Request(id, clientId, clientName, clientPicPath,
                                  originalLang, targetLang, isSos, format,
                                  subject, type, content, context, status,
                                  registeredTime, dueTime, expectedTime,
                                  words, points, title);

            request.translatorsInQueue = parseProfileList(jsonObject.getJSONArray("request_translatorsInQueue"));
            request.translation = parseTranslation(jsonObject);

        } catch (JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
            e.printStackTrace();
        } catch (ParseException e) {
            Log.e(DEBUG_TAG, e.getMessage());
            e.printStackTrace();
        }

        return request;
    }

    public static ArrayList<Translation> parseTranslationList(JSONArray jsonArray) {
        ArrayList<Translation> translations = new ArrayList<Translation>();
        int size = jsonArray.length();
        try {
            for (int i = 0; i < size; i++) {
                translations.add(parseTranslation(jsonArray.getJSONObject(i)));
            }
        } catch (JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
            e.printStackTrace();
        }
        return translations;
    }

    public static Translation parseTranslation(JSONObject jsonObject) {
        Translation translation = null;

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

            String id = jsonObject.getString("request_translatorId");
            String name = jsonObject.getString("request_translatorName");
            String picPath = jsonObject.getString("request_translatorPicPath");
            String badgeList = jsonObject.getString("request_translatorBadgeList");

            int[] badges = null;
            if (badgeList != null) {
                String[] badgeArr = badgeList.split(",");
                badges = new int[badgeArr.length];
                for (int i = 0 ; i < badgeArr.length;i++) {
                    badges[i] = Integer.parseInt(badgeArr[i]);
                }
            }
            Profile translatorProfile = new Profile(id, name, picPath, badges);

            String content = jsonObject.getString("request_translatedText");
            String comment = jsonObject.getString("request_translatorComment");
            Date   submittedTime = dateFormat.parse(jsonObject.getString("request_submittedTime"));
            int    tone = jsonObject.getInt("request_translatedTone");
            int    feedbackScore = jsonObject.getInt("request_feedbackScore");

            translation = new Translation(translatorProfile,
                                          content, comment, submittedTime,
                                          tone, feedbackScore);
        } catch (JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
            e.printStackTrace();
        } catch (ParseException e) {
            Log.e(DEBUG_TAG, e.getMessage());
            e.printStackTrace();
        }
        return translation;
    }

    public static ArrayList<Profile> parseProfileList(JSONArray jsonArray) {
        ArrayList<Profile> profiles = new ArrayList<Profile>();
        int size = jsonArray.length();
        try {
            for (int i = 0; i < size; i++) {
                profiles.add(parseProfile(jsonArray.getJSONObject(i)));
            }
        } catch (JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
            e.printStackTrace();
        }
        return profiles;
    }

    public static Profile parseProfile(JSONObject jsonObject) {
        Profile profile = null;

        try {
            String id = jsonObject.getString("user_email");
            String name = jsonObject.getString("user_name");
            String profilePicPath = jsonObject.getString("user_profilePicPath");
            int    category = jsonObject.getInt("user_isTranslator");
            int motherLang = jsonObject.getInt("user_motherLang");
            String transLang = jsonObject.getString("user_translatableLang");
            int[] translatableLang = null;
            if (transLang != null) {
                String[] transArr = transLang.split(",");
                translatableLang = new int[transArr.length];
                for (int i = 0 ; i < transArr.length;i++) {
                    translatableLang[i] = Integer.parseInt(transArr[i]);
                }
            }

            String badges = jsonObject.getString("user_badgeList");
            int[] badgeList = null;
            if (badges != null) {
                String[] badgeArr = badges.split(",");
                badgeList = new int[badgeArr.length];
                for (int i = 0 ; i < badgeArr.length; i++) {
                    badgeList[i] = Integer.parseInt(badgeArr[i]);
                }
            }

            int numOfPendingRequests = jsonObject.getInt("user_numOfRequestsPending");
            int numOfOngoingRequests = jsonObject.getInt("user_numOfRequestsOngoing");
            int numOfCompleteRequests = jsonObject.getInt("user_numOfRequestsCompleted");
            int numOfPendingTranslations = jsonObject.getInt("user_numOfTranslationsPending");
            int numOfOngoingTranslations = jsonObject.getInt("user_numOfTranslationsOngoing");
            int numOfCompleteTranslations = jsonObject.getInt("user_numOfTranslationsCompleted");

            double revenue = jsonObject.getDouble("user_revenue");
            String profileText = jsonObject.getString("user_profileText");

            profile = new Profile(id, name, profilePicPath, category,
                                  motherLang, translatableLang, badgeList,
                                  numOfPendingRequests, numOfOngoingRequests, numOfCompleteRequests,
                                  numOfPendingTranslations, numOfOngoingTranslations, numOfCompleteTranslations,
                                  revenue, profileText);
        } catch (JSONException e) {
            Log.e(DEBUG_TAG, e.getMessage());
            e.printStackTrace();
        }
        return profile;
    }

}
