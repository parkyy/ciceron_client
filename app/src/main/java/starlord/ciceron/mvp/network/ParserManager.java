/*
 *  XML pull parser�� ���ϴ� ��ü
 *  author PYO IN SOO
 */
package starlord.ciceron.mvp.network;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.annotation.SuppressLint;
import android.util.JsonReader;
import android.util.Log;

public class ParserManager {
	private  static  XmlPullParser  pullParser;
    private  static  XmlPullParserFactory  parserFactory;		// Factory ��ü�� �� �ѹ�.
    private  static  JsonReader jsonReader;
    private  static  String  DEBUG_TAG = "ParserManager";

	/*
	 *  SAX Parser��  ���Ѵ�.
	 */
	public static XmlPullParser generateXMLPullParser(){
		if(pullParser != null){
			return pullParser;
		} else {
			try {
			  parserFactory = XmlPullParserFactory.newInstance();
			  pullParser = parserFactory.newPullParser();
			} catch(XmlPullParserException xppe){
				Log.e(DEBUG_TAG, "generateXMLPullParser() ", xppe);
			}
			return pullParser;
		}
	}
    /*
	 * �ϳ��� Ǯ�ļ� ��ü�� ����ؼ� �������ִ� �޼ҵ�
	 */
	public static XmlPullParser realGeneratePullParser(){
		try{
			if( parserFactory == null )  {
				parserFactory = XmlPullParserFactory.newInstance();
		    	return  parserFactory.newPullParser();
		     } else{
		    	return parserFactory.newPullParser();
		     }
		} catch(XmlPullParserException xppe){
			Log.e(DEBUG_TAG, "realGeneratePullParser() ", xppe);
		}
		return  null;
	}
	
	@SuppressLint("NewApi")
	public static JsonReader generateJsonReader(InputStream instream) {
		if (jsonReader != null)	jsonReader = null;
		jsonReader = new JsonReader(new InputStreamReader(instream));
		return jsonReader;
	}
}