/*
 *  �پ��� HTTP �����? ���ϴ� ��ü
 *  author PYO IN SOO
 */
package starlord.ciceron.mvp.network;


import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Protocol;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import starlord.ciceron.mvp.common.CiceronApp;
import starlord.ciceron.mvp.common.PersistentCookieStore;

public class HttpConnManager {
    private static OkHttpClient httpClient;

    public static OkHttpClient getInstance() {
        if (httpClient == null) {
            httpClient = new OkHttpClient();
            httpClient.setConnectTimeout(NetworkConfig.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
            httpClient.setReadTimeout(NetworkConfig.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);

            try {
                Protocol protocol = Protocol.get("http/1.1");
                List<Protocol> protocols = new ArrayList<Protocol>();
                protocols.add(protocol);
                httpClient.setProtocols(protocols);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Cookie Setting //
            httpClient.setCookieHandler(new CookieManager(
                    new PersistentCookieStore(CiceronApp.getAppInstance().getAppContext()),
                    CookiePolicy.ACCEPT_ALL));

            httpClient.setRetryOnConnectionFailure(false);


            return httpClient;
        }
        return httpClient;
    }
}
