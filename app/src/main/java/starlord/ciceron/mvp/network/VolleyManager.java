package starlord.ciceron.mvp.network;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.util.LruCache;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import starlord.ciceron.mvp.common.CiceronApp;
import starlord.ciceron.mvp.image.ImageCache;

/**
 * Created by JaehongPark on 15. 4. 18..
 */
public class VolleyManager {

    private static VolleyManager instance = null;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;
    private static final String DEBUG_TAG = "VolleyRequest";

    private VolleyManager() {
        requestQueue = getRequestQueue();

        imageLoader = new ImageLoader(requestQueue,
                new ImageLoader.ImageCache() {

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        // TODO Auto-generated method stub
                        ImageCache.getInstance(null).addBitmapToCache(url, bitmap);
                    }

                    @Override
                    public Bitmap getBitmap(String url) {
                        // TODO Auto-generated method stub
                        return ImageCache.getInstance(null).getBitmapFromMemCache(url);
                    }
                });
    }

    public static synchronized VolleyManager getInstance() {
        if (instance == null) {
            instance = new VolleyManager();
        }
        return instance;
    }

    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(CiceronApp.getAppInstance().getAppContext());
        }
        return requestQueue;
    }

    public ImageLoader getImageLoader() {
        return imageLoader;
    }


    /**
     * Adds the specified request to the global queue, if tag is specified
     * then it is used else Default TAG is used.
     *
     * @param req
     * @param tag
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? DEBUG_TAG : tag);
        VolleyLog.d("Adding request to queue: %s", req.getUrl());
        getRequestQueue().add(req);
    }

    /**
     * Adds the specified request to the global queue using the Default TAG.
     *
     * @param req
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(DEBUG_TAG);
        VolleyLog.d("Adding request to queue: %s", req.getUrl());
        getRequestQueue().add(req);
    }


    /**
     * Cancels all pending requests by the specified TAG, it is important
     * to specify a default TAG so that the pending/ongoing requests can be cancelled.
     */
    public void cancelPendingRequests() {
        if (requestQueue != null) {
            requestQueue.cancelAll(DEBUG_TAG);
        }
    }
}
