package starlord.ciceron.mvp.network;

/**
 * Created by JaehongPark on 15. 5. 16..
 */
public class NetResponse {
    /* WorkerMainActivity Response */
    public static final int ADD_QUEUE_OK = 200;
    public static final int NO_TRANSLATOR_AUTH = 403;
    public static final int NO_LANG_PERMISSION = 401;
    public static final int NOT_ACCESSABLE_REQ = 406;
    public static final int ALREADY_IN_QUEUE = 204;

    public static final int INIT_TRANS_OK = 201;
    public static final int ALREADY_TAKEN_REQ = 410;

    public static final int DEL_REQUEST_OK = 202;

    public static final int OTHERS = 0;
}
