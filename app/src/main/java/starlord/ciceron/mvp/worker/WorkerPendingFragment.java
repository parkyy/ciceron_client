package starlord.ciceron.mvp.worker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.ReqPending;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.RequestAdapter;
import starlord.ciceron.mvp.main.BaseListFragment;
import starlord.ciceron.mvp.request.common.ReqOverview;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WorkerPendingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WorkerPendingFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class WorkerPendingFragment extends BaseListFragment {

    private ArrayList<Request> requestList;
    private RequestAdapter requestAdapter;

    private static final String DEBUG_TAG = "[WorkerPendingFragment]";

    public static WorkerPendingFragment newInstance() {
        return new WorkerPendingFragment();
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WorkerPendingFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_worker_pending, container, false);
        initFrag(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(requestAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
        requestAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onListItemClick(ListView l, View v, int index, long id) {
        super.onListItemClick(l, v, index, id);
        if (requestAdapter != null) {
            showRequestDetailDialog(requestAdapter.getItem(index));
        }
    }

    //@Override
    public void initFrag(View view) {

        // TODO: Modify this:
        requestList = (ArrayList<Request>) ReqPending.getList();
        requestAdapter = new RequestAdapter(getActivity(), requestList);
    }

    private void showRequestDetailDialog(final Request request) {
        ReqOverview requestOverview = new ReqOverview(getActivity());
        requestOverview.setLayout(request);
        requestOverview.hideContent();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(requestOverview)
                .setPositiveButton("번역하기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        initTranslation(request);
                    }
                })
                .setNegativeButton("삭제하기", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deQueueTranslation(request);
                    }
                });

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button btnTranslate = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                btnTranslate.setBackgroundColor(getResources().getColor(R.color.darkblue));
                btnTranslate.setTextColor(getResources().getColor(R.color.white));

                Button btnCancel = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                btnCancel.setBackgroundColor(getResources().getColor(R.color.scarlet));
                btnCancel.setTextColor(getResources().getColor(R.color.white));

//                btnMsg.setCompoundDrawables(null);
            }
        });
        dialog.show();
    }


    private void initTranslation(final Request request) {
        if(mListener != null)
            mListener.initTranslation(request);
    }

    private void deQueueTranslation(final Request request) {
        if(mListener != null)
            mListener.deQueueTranslation(request);
    }

}
