package starlord.ciceron.mvp.worker;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.main.BaseListFragment;
import starlord.ciceron.mvp.translate.TransCheckActivity;
import starlord.ciceron.mvp.translate.TransMainActivity;

public class WorkerMainActivity extends BaseActivity implements BaseListFragment.OnInteractionListener {

    private FragmentManager fm;

    private WorkerNewsFragment     newsFrag;
    private WorkerPendingFragment  pendingFrag;
    private WorkerOngoingFragment  ongoingFrag;
    private WorkerCompleteFragment completeFrag;

    private Button tabNews;
    private Button tabPending;
    private Button tabOngoing;
    private Button tabComplete;

    private static final int       POST_TRANSLATION = 0;
    private static final int       CHECK_COMPLETE_TRANSLATION = 1;

    private static final String    DEBUG_TAG = "[WorkerMainActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_worker_main);
        Log.d(DEBUG_TAG, "onCreate");
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(DEBUG_TAG, "onDestroy");
    }

    //@Override
    protected void initView() {
        fm = getFragmentManager();

        newsFrag = WorkerNewsFragment.newInstance();
        pendingFrag = WorkerPendingFragment.newInstance();
        ongoingFrag = WorkerOngoingFragment.newInstance();
        completeFrag = WorkerCompleteFragment.newInstance();

        tabNews = (Button) findViewById(R.id.tab_worker_news);
        tabNews.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCurrentModeTo(R.id.tab_worker_news);
            }
        });
        tabPending = (Button) findViewById(R.id.tab_worker_pending);
        tabPending.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCurrentModeTo(R.id.tab_worker_pending);
            }
        });
        tabOngoing = (Button) findViewById(R.id.tab_worker_ongoing);
        tabOngoing.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCurrentModeTo(R.id.tab_worker_ongoing);
            }
        });
        tabComplete = (Button) findViewById(R.id.tab_worker_complete);
        tabComplete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCurrentModeTo(R.id.tab_worker_complete);
            }
        });

        switchCurrentModeTo(R.id.tab_worker_news);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_worker_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.btn_menu_worker_main) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            // TODO: Modify This
        }

    }

    private void switchCurrentModeTo (int btnId){
        FragmentTransaction ft = fm.beginTransaction();
        switch(btnId) {
            case R.id.tab_worker_news:
                ft.replace(R.id.frame_worker_main, newsFrag);
                actionBar.setTitle("뉴스피드");
                break;

            case R.id.tab_worker_pending:
                ft.replace(R.id.frame_worker_main, pendingFrag);
                actionBar.setTitle("장바구니");
                break;

            case R.id.tab_worker_ongoing:
                ft.replace(R.id.frame_worker_main, ongoingFrag);
                actionBar.setTitle("작업중인번역");
                break;

            case R.id.tab_worker_complete:
                ft.replace(R.id.frame_worker_main, completeFrag);
                actionBar.setTitle("활동기록");
                break;
        }
        ft.commit();
    }

    public void initRequest() {

    }

    public void reviewRequest(Request request) {

    }

    public void checkCompleteRequest(Request request) {

    }

    @Override
    public void cancelRequest(Request request) {

    }

    @Override
    public void initTranslation(Request request) {
        Intent intent = new Intent(this, TransMainActivity.class);
        if(request != null) {
            Bundle reqBundle = request.toBundle();
            intent.putExtras(reqBundle);
        }
        startActivityForResult(intent, POST_TRANSLATION);
    }

    @Override
    public void queueTranslation(Request request) {

    }

    @Override
    public void deQueueTranslation(Request request) {

    }

    @Override
    public void checkCompleteTranslation(Request request) {
        Intent intent = new Intent(this, TransCheckActivity.class);
        if(request != null) {
            Bundle reqBundle = request.toBundle();
            intent.putExtras(reqBundle);
        }
        startActivityForResult(intent, CHECK_COMPLETE_TRANSLATION);
    }


}
