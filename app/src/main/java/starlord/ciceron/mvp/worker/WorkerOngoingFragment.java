package starlord.ciceron.mvp.worker;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.ReqOngoing;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.RequestAdapter;
import starlord.ciceron.mvp.main.BaseListFragment;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link starlord.ciceron.mvp.worker.WorkerOngoingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link starlord.ciceron.mvp.worker.WorkerOngoingFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class WorkerOngoingFragment extends BaseListFragment {

    private ArrayList<Request> requestList;
    private RequestAdapter requestAdapter;

    private static final String DEBUG_TAG = "[WorkerOngoingFragment]";

    public static WorkerOngoingFragment newInstance() {
        return new WorkerOngoingFragment();
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public WorkerOngoingFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_worker_ongoing, container, false);
        initFrag(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(requestAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
        requestAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onListItemClick(ListView l, View v, int index, long id) {
        super.onListItemClick(l, v, index, id);
        if (requestAdapter != null) {
            resumeTranslation(requestAdapter.getItem(index));
        }

    }

    //@Override
    public void initFrag(View view) {
        // TODO : Modify this:
        requestList = ReqOngoing.getList();
        requestAdapter = new RequestAdapter(getActivity(), requestList);
    }

    private void resumeTranslation(final Request request) {
        if(mListener != null)
            mListener.initTranslation(request);
    }
}
