package starlord.ciceron.mvp.client;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.ReqPending;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.RequestAdapter;
import starlord.ciceron.mvp.main.BaseListFragment;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link starlord.ciceron.mvp.client.ClientOrderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link starlord.ciceron.mvp.client.ClientOrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ClientOrderFragment extends BaseClientFragment {

    private Button  btnNewSos;
    private Button  btnNewRequest;

    private ArrayList<Request> requestList;
    private RequestAdapter requestAdapter;

    private static final String DEBUG_TAG = "[ClientOrderFragment]";

    public static ClientOrderFragment newInstance() {
        return new ClientOrderFragment();
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ClientOrderFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_client_order, container, false);
        initFrag(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(requestAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
        requestAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onListItemClick(ListView l, View v, int index, long id) {
        super.onListItemClick(l, v, index, id);
        if (requestAdapter != null) {
            showRequestDetailDialog(requestAdapter.getItem(index));
        }
    }

    //@Override
    public void initFrag(View view) {
        btnNewRequest = (Button) view.findViewById(R.id.btn_newOrder_client);
        btnNewRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initRequest();
            }
        });

        // TODO: Modify this:
        requestList = (ArrayList<Request>) ReqPending.getList();
        requestAdapter = new RequestAdapter(getActivity(), requestList);
    }

    private void showRequestDetailDialog(final Request request) {
/*        RequestOverview requestOverview = new RequestOverview(context);
        requestOverview.setLayout(request);
        requestOverview.hideContent();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(requestOverview)
                .setPositiveButton("Translate Now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        initTranslation(request);
                    }
                })
                .setNegativeButton(" Add to list ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        addToQueue(request);
                    }
                });

        builder.create().show();*/
    }


    private void initRequest() {
        if(mListener != null)
            mListener.initRequest();
    }



}
