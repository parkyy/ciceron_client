package starlord.ciceron.mvp.client;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.main.BaseActivity;
import starlord.ciceron.mvp.request.check.ReqCheckActivity;
import starlord.ciceron.mvp.request.check.ReqEditActivity;
import starlord.ciceron.mvp.request.create.ReqMainActivity;
import starlord.ciceron.mvp.worker.WorkerMainActivity;

public class ClientMainActivity extends BaseActivity implements BaseClientFragment.OnClientInteractionListener {

    private FragmentManager fm;

    private ClientStoaFragment newsFrag;
    private ClientOrderFragment    orderFrag;
    private ClientOngoingFragment  ongoingFrag;
    private ClientCompleteFragment completeFrag;

    private Button tabNews;
    private Button tabPending;
    private Button tabOngoing;
    private Button tabComplete;

    private static final int POST_NEW_REQUEST = 0;
    private static final int REVIEW_ONGOING_REQUEST = 1;
    private static final int CHECK_COMPLETE_REQUEST = 2;

    private static final String    DEBUG_TAG = "[ClientMainActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_main);
        Log.d(DEBUG_TAG, "onCreate");
        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(DEBUG_TAG, "onDestroy");
    }

    //@Override
    protected void initView() {
        fm = getFragmentManager();

        newsFrag = ClientStoaFragment.newInstance();
        orderFrag = ClientOrderFragment.newInstance();
        ongoingFrag = ClientOngoingFragment.newInstance();
        completeFrag = ClientCompleteFragment.newInstance();

        tabNews = (Button) findViewById(R.id.tab_client_stoa);
        tabNews.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCurrentModeTo(R.id.tab_client_stoa);
            }
        });

        tabPending = (Button) findViewById(R.id.tab_client_order);
        tabPending.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCurrentModeTo(R.id.tab_client_order);
            }
        });
        tabOngoing = (Button) findViewById(R.id.tab_client_ongoing);
        tabOngoing.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCurrentModeTo(R.id.tab_client_ongoing);
            }
        });
        tabComplete = (Button) findViewById(R.id.tab_client_complete);
        tabComplete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                switchCurrentModeTo(R.id.tab_client_complete);
            }
        });

        switchCurrentModeTo(R.id.tab_client_stoa);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_client_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.btn_menu_client_main) {
            Intent intent = new Intent(this, WorkerMainActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            // TODO: Modify This
        }

    }

    private void switchCurrentModeTo (int btnId){
        FragmentTransaction ft = fm.beginTransaction();
        switch(btnId) {
            case R.id.tab_client_stoa:
                ft.replace(R.id.frame_client_main, newsFrag);
                actionBar.setTitle("뉴스피드");
                break;

            case R.id.tab_client_order:
                ft.replace(R.id.frame_client_main, orderFrag);
                actionBar.setTitle("의뢰하기");
                break;

            case R.id.tab_client_ongoing:
                ft.replace(R.id.frame_client_main, ongoingFrag);
                actionBar.setTitle("처리상황");
                break;

            case R.id.tab_client_complete:
                ft.replace(R.id.frame_client_main, completeFrag);
                actionBar.setTitle("완료된 번역");
                break;
        }
        ft.commit();
    }

    @Override
    public void initRequest() {
        Intent intent = new Intent(this, ReqMainActivity.class);
        startActivityForResult(intent, POST_NEW_REQUEST);
    }

    @Override
    public void reviewRequest(Request request) {
        if (request != null) {
            Intent intent = new Intent(this, ReqEditActivity.class);
            intent.putExtras(request.toBundle());
            startActivityForResult(intent, REVIEW_ONGOING_REQUEST);
        }
    }

    @Override
    public void cancelRequest(Request request) {

    }

    @Override
    public void checkCompleteRequest(Request request) {
        if(request != null) {
            Intent intent = new Intent(this, ReqCheckActivity.class);
            intent.putExtras(request.toBundle());
            startActivityForResult(intent, CHECK_COMPLETE_REQUEST);
        }
    }

}
