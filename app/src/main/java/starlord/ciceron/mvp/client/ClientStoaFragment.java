package starlord.ciceron.mvp.client;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.NewsFeedList;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.RequestAdapter;
import starlord.ciceron.mvp.network.ResponseJSONParser;
import starlord.ciceron.mvp.request.common.ReqOverview;
import starlord.ciceron.mvp.request.common.ReqSosView;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ClientStoaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ClientStoaFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ClientStoaFragment extends BaseClientFragment {

    private Button  btnNewSos;

    private ArrayList<Request> requestList;
    private RequestAdapter     requestAdapter;

    private static final String DEBUG_TAG = "[ClientStoaFragment]";

    public static ClientStoaFragment newInstance() {
        return new ClientStoaFragment();
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ClientStoaFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_client_news, container, false);
        initFrag(root);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(requestAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(DEBUG_TAG, "onResume");
        requestAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onListItemClick(ListView l, View v, int index, long id) {
        super.onListItemClick(l, v, index, id);
        if (requestAdapter != null) {
            showRequestDetailDialog(requestAdapter.getItem(index));
        }
    }

    //@Override
    public void initFrag(View view) {
        btnNewSos = (Button) view.findViewById(R.id.btn_newSOS_client);
        btnNewSos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showNewSosDialog();
            }
        });

        // TODO: Modify this:
        requestList = NewsFeedList.getList();
        requestAdapter = new RequestAdapter(getActivity(), requestList);
    }

    private void getRequests() {
        synchronized (requestAdapter) {
            //requestList = ResponseJSONParser.parseRequestList();
        }
    }


    private void showRequestDetailDialog(final Request request) {
        ReqOverview requestOverview = new ReqOverview(getActivity());
        requestOverview.setLayout(request);
        requestOverview.hideContent();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(requestOverview)
                .setPositiveButton("번역은 번역가 모드에서 가능합니다", null);

        final AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                Button btnMsg = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
                btnMsg.setBackgroundColor(getResources().getColor(R.color.scarlet));
                btnMsg.setTextColor(getResources().getColor(R.color.white));
            }
        });
        dialog.show();
    }


    private void showNewSosDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(new ReqSosView(getActivity()));

        builder.create().show();
    }



}
