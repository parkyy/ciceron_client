package starlord.ciceron.mvp.main;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.TransAdapter;
import starlord.ciceron.mvp.collection.TransComplete;
import starlord.ciceron.mvp.collection.TransProgress;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TransFragment extends BaseListFragment implements View.OnClickListener {

    private Button  btnInComplete;
    private Button  btnComplete;

    private TransAdapter incompleteAdapter;
    private TransAdapter completeAdapter;

    private int STATUS;
    private static final int STATUS_INCOMPLETE = 200;
    private static final int STATUS_COMPLETED = 201;

    private static final String DEBUG_TAG = "[TransFragment]";

    public static TransFragment newInstance() {
        return new TransFragment();
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TransFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        if (getArguments() != null) {

        }
        Activity activity = getActivity();

        incompleteAdapter = new TransAdapter(activity, R.layout.item_list_req, TransProgress.getList());
        completeAdapter = new TransAdapter(activity, R.layout.item_list_req, TransComplete.getList());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_trans, container, false);

        btnInComplete = (Button) root.findViewById(R.id.tab_incomplete_trans);
        btnComplete = (Button) root.findViewById(R.id.tab_complete_trans);

        btnInComplete.setOnClickListener(this);
        btnComplete.setOnClickListener(this);

        switchCurrentTabTo(STATUS_INCOMPLETE);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(incompleteAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        incompleteAdapter.notifyDataSetChanged();
        completeAdapter.notifyDataSetChanged();

        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Request request;

        switch (STATUS) {
            case STATUS_INCOMPLETE:
                request = incompleteAdapter.getItem(position);
                initTranslation(request);
                break;

            case STATUS_COMPLETED:
                request = completeAdapter.getItem(position);
                reviewCompleteTranslation(request);
                break;
        }
    }

    private void initTranslation(final Request request) {
        if(request != null) {
            if (mListener != null)
                mListener.initTranslation(request);
        }
    }

    private void reviewCompleteTranslation(final Request request) {
        if (request != null) {
            if (mListener != null)
                mListener.reviewRequest(request);
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {

            case R.id.tab_incomplete_trans:
                switchCurrentTabTo(STATUS_INCOMPLETE);
                btnInComplete.setSelected(true);
                btnComplete.setSelected(false);
                break;

            case R.id.tab_complete_trans:
                switchCurrentTabTo(STATUS_COMPLETED);
                btnInComplete.setSelected(false);
                btnComplete.setSelected(true);
                break;
        }
    }


    private void switchCurrentTabTo(final int mode) {
        switch(mode) {
            case STATUS_INCOMPLETE:
                STATUS = STATUS_INCOMPLETE;
                incompleteAdapter.notifyDataSetChanged();
                setListAdapter(incompleteAdapter);

                btnInComplete.setSelected(true);
                btnComplete.setSelected(false);
                break;

            case STATUS_COMPLETED:
                STATUS = STATUS_COMPLETED;
                completeAdapter.notifyDataSetChanged();
                setListAdapter(completeAdapter);

                btnInComplete.setSelected(false);
                btnComplete.setSelected(true);
                break;
        }
    }


}
