package starlord.ciceron.mvp.main;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.ReqComplete;
import starlord.ciceron.mvp.collection.ReqPending;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.RequestAdapter;


/**
 * A simple {@link android.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrderFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrderFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class OrderFragment extends BaseListFragment implements View.OnClickListener {

    private Button  btnInComplete;
    private Button  btnComplete;
    private Button  btnNewRequest;

    private RequestAdapter incompleteAdapter;
    private RequestAdapter completeAdapter;

    private int STATUS;
    private static final int STATUS_INCOMPLETE = 100;
    private static final int STATUS_COMPLETED = 101;

    private static final String DEBUG_TAG = "[OrderFragment]";

    public static OrderFragment newInstance() {
        return new OrderFragment();
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OrderFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");


        if (getArguments() != null) {

        }
        Activity activity = getActivity();

        incompleteAdapter  = new RequestAdapter(activity, ReqPending.getList());
        completeAdapter = new RequestAdapter(activity, ReqComplete.getList());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_order, container, false);

        btnInComplete   = (Button) root.findViewById(R.id.tab_incomplete_order);
        btnComplete   = (Button) root.findViewById(R.id.tab_complete_order);
        btnNewRequest = (Button) root.findViewById(R.id.btn_neworder);

        btnInComplete.setOnClickListener(this);
        btnComplete.setOnClickListener(this);
        btnNewRequest.setOnClickListener(this);

        switchCurrentTabTo(STATUS_INCOMPLETE);
        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
        setListAdapter(incompleteAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        incompleteAdapter.notifyDataSetChanged();
        completeAdapter.notifyDataSetChanged();

        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Request request = (Request) getListAdapter().getItem(position);
        switch (STATUS) {
            case STATUS_INCOMPLETE:
                reviewRequest(request);
                break;

            case STATUS_COMPLETED:
                checkCompleteRequest(request);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {

            case R.id.tab_incomplete_order:
                switchCurrentTabTo(STATUS_INCOMPLETE);
                break;

            case R.id.tab_complete_order:
                switchCurrentTabTo(STATUS_COMPLETED);
                break;

            case R.id.btn_neworder:
                initRequest();
                break;
        }
    }

    private void initRequest() {
        if(mListener != null)
            mListener.initRequest();
    }

    private void reviewRequest(final Request request) {
        if (request != null) {
            if (mListener != null)
                mListener.reviewRequest(request);
        }
    }

    private void checkCompleteRequest(final Request request) {
        if(request != null) {
            if(request.isCompleted()) {
                if (mListener != null)
                    mListener.checkCompleteRequest(request);
            }
        }
    }

    private void switchCurrentTabTo(final int mode) {
        switch(mode) {
            case STATUS_INCOMPLETE:
                STATUS = STATUS_INCOMPLETE;
                incompleteAdapter.notifyDataSetChanged();
                setListAdapter(incompleteAdapter);

                btnInComplete.setSelected(true);
                btnComplete.setSelected(false);
                break;

            case STATUS_COMPLETED:
                STATUS = STATUS_COMPLETED;
                completeAdapter.notifyDataSetChanged();
                setListAdapter(completeAdapter);

                btnInComplete.setSelected(false);
                btnComplete.setSelected(true);
                break;
        }
    }



}
