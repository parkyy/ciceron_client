package starlord.ciceron.mvp.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.Request.Method;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import starlord.ciceron.mvp.R;
import starlord.ciceron.mvp.collection.NewsFeedList;
import starlord.ciceron.mvp.network.NetworkAPI;
import starlord.ciceron.mvp.network.ResponseJSONParser;
import starlord.ciceron.mvp.network.ServerStatusRequest;
import starlord.ciceron.mvp.network.VolleyManager;
import starlord.ciceron.mvp.request.common.ReqOverview;
import starlord.ciceron.mvp.collection.NewsFeedAdapter;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.TransPending;

/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class NewsFragment extends BaseListFragment {
    private Context            context;
    private ArrayList<Request> newsFeedList;
    private NewsFeedAdapter    newsFeedAdapter;


    private static final String DEBUG_TAG = "[NewListFragment]";

    public static NewsFragment newInstance() {
        return new NewsFragment();
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public NewsFragment() {

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity.getBaseContext();
        Log.d(DEBUG_TAG, "onAttach");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(DEBUG_TAG, "onCreate");

        newsFeedAdapter = new NewsFeedAdapter(getActivity(), NewsFeedList.getList());
        setListAdapter(newsFeedAdapter);
        /*if(newsFeedList == null) {
            loadNewsFeedList();
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(DEBUG_TAG, "onCreateView");
        View root = inflater.inflate(R.layout.fragment_newsfeed, container, false);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(DEBUG_TAG, "onActivityCreated");
    }

    @Override
    public void onResume() {
        super.onResume();

        if (newsFeedAdapter != null)
            newsFeedAdapter.notifyDataSetChanged();
        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(DEBUG_TAG, "onSaveInstanceState");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.d(DEBUG_TAG, "onDetach");
    }

    @Override
    public void onListItemClick(ListView l, View v, int index, long id) {
        super.onListItemClick(l, v, index, id);
        popTranslateDialog(newsFeedAdapter.getItem(index));
    }

    private void loadNewsFeedList() {

        JsonArrayRequest request = new JsonArrayRequest(
                NetworkAPI.NEWSFEED_API, new Response.Listener<JSONArray> () {

            @Override
            public void onResponse(JSONArray response) {
                if (response != null) {
                    Log.d(DEBUG_TAG, "newsFeedResponse: " + response.toString());
                    newsFeedList = ResponseJSONParser.parseRequestList(response);
                    if (newsFeedAdapter == null) {
                        newsFeedAdapter = new NewsFeedAdapter(context, newsFeedList);
                        setListAdapter(newsFeedAdapter);
                    }
                    newsFeedAdapter.notifyDataSetChanged();
                    System.gc();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(DEBUG_TAG, "newsFeedResponse: " + error.toString());
                newsFeedList = new ArrayList<Request>();
                if (newsFeedAdapter == null) {
                    newsFeedAdapter = new NewsFeedAdapter(context, newsFeedList);
                    setListAdapter(newsFeedAdapter);
                }
                newsFeedAdapter.notifyDataSetChanged();
                error.printStackTrace();
                System.gc();
            }
        });
        // add the request object to the queue to be executed
        VolleyManager.getInstance().addToRequestQueue(request);
    }

    private void popTranslateDialog(final Request request) {
        ReqOverview requestOverview = new ReqOverview(context);
        requestOverview.setLayout(request);
        requestOverview.hideContent();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(requestOverview)
                .setPositiveButton("Translate Now", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        initTranslation(request);
                    }
                })
                .setNegativeButton(" Add to list ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        addToQueue(request);
                    }
                });

        builder.create().show();
    }

    private void handleHttpResponse(int statusCode) {
        if (statusCode == HttpURLConnection.HTTP_OK) {
            // REQUEST ADDED TO QUEUE
        } else if (statusCode == HttpURLConnection.HTTP_NOT_ACCEPTABLE) {
            // NOT ABLE TO TRANSLATE
        } else {

        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder
                .setView(null);

        builder.create().show();
    }


    private void addToQueue(final Request request) {
        final String requestId = String.valueOf(request.id);
        ServerStatusRequest signUpRequest = new ServerStatusRequest(
                Method.POST,
                NetworkAPI.USER_TRANSLATION_PENDING_API,

                new Response.Listener<Integer>() {
                    @Override
                    public void onResponse(Integer statusCode) {
                        handleHttpResponse(statusCode);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleHttpResponse(-1);
                        error.printStackTrace();
                    }
                })
            {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("request_id", requestId);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };
        // Add Request to Network Request Queue
        VolleyManager.getInstance().addToRequestQueue(signUpRequest);

        // TODO : remove this
        TransPending.addRequest(request);
    }

    private void initTranslation(final Request request) {
        final String requestId = String.valueOf(request.id);
        ServerStatusRequest signUpRequest = new ServerStatusRequest(
                Method.POST,
                NetworkAPI.USER_TRANSLATION_ONGOING_API,

                new Response.Listener<Integer>() {
                    @Override
                    public void onResponse(Integer statusCode) {
                        if (statusCode == HttpURLConnection.HTTP_OK) {
                            mListener.initTranslation(request);
                        } else {
                            handleHttpResponse(statusCode);
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        handleHttpResponse(-1);
                        error.printStackTrace();
                    }
                })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("request_id", requestId);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type","application/x-www-form-urlencoded");
                return headers;
            }
        };
        // Add Request to Network Request Queue
        VolleyManager.getInstance().addToRequestQueue(signUpRequest);


        // TODO : remove this
        if(mListener != null)
            mListener.initTranslation(request);
    }

}
