package starlord.ciceron.mvp.main;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


import starlord.ciceron.mvp.R;

import starlord.ciceron.mvp.request.check.ReqCheckActivity;
import starlord.ciceron.mvp.request.check.ReqEditActivity;
import starlord.ciceron.mvp.request.create.ReqMainActivity;
import starlord.ciceron.mvp.collection.Request;
import starlord.ciceron.mvp.collection.SectionsPagerAdapter;
import starlord.ciceron.mvp.translate.TransCheckActivity;
import starlord.ciceron.mvp.translate.TransMainActivity;


public class MainActivity extends BaseActivity implements BaseListFragment.OnInteractionListener {


    private ViewPager              mViewPager;
    private SectionsPagerAdapter   mPagerAdapter;

    private static final int       POST_REQUEST = 0;
    private static final int       REVIEW_REQUEST = 1;
    private static final int       CHECK_COMPLETE_REQUEST = 2;

    private static final int       POST_TRANSLATION = 3;
    private static final int       CHECK_COMPLETE_TRANSLATION = 4;

    private static final String    DEBUG_TAG = "[MainActivity]";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(DEBUG_TAG, "onCreate");

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setLogo(null);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mPagerAdapter = new SectionsPagerAdapter(this, mViewPager);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("뉴스피드")
                        .setIcon(R.drawable.ic_tab_alerts),
                         NewsFragment.class, null);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("의뢰하기")
                        .setIcon(R.drawable.ic_tab_request),
                        OrderFragment.class, null);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("번역하기")
                        .setIcon(R.drawable.ic_tab_translates),
                        TransFragment.class, null);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("알림")
                        .setIcon(R.drawable.ic_tab_alerts),
                        StatusFragment.class, null);

        mPagerAdapter.addTab(actionBar.newTab()
                        .setContentDescription("설정")
                        .setIcon(R.drawable.ic_tab_settings),
                        ConfigFragment.class, null);

        if (savedInstanceState != null) {
            actionBar.setSelectedNavigationItem(savedInstanceState.getInt("tab", 0));
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        Log.d(DEBUG_TAG, "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(DEBUG_TAG, "onPause");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tab", getActionBar().getSelectedNavigationIndex());
    }

    public void initRequest() {
        Intent intent = new Intent(this, ReqMainActivity.class);
        startActivityForResult(intent, POST_REQUEST);
    }

    public void reviewRequest(Request request) {
        Intent intent = new Intent(this, ReqEditActivity.class);
        if(request != null) {
            intent.putExtras(request.toBundle());
        }
        startActivityForResult(intent, REVIEW_REQUEST);
    }

    @Override
    public void cancelRequest(Request request) {

    }

    public void checkCompleteRequest(Request request) {
        if(request != null) {
            Bundle reqBundle = request.toBundle();
            Intent intent = new Intent(this, ReqCheckActivity.class);
            intent.putExtras(reqBundle);
            startActivityForResult(intent, CHECK_COMPLETE_REQUEST);
        }
    }

    public void initTranslation(Request request) {
        Intent intent = new Intent(this, TransMainActivity.class);
        if(request != null) {
            Bundle reqBundle = request.toBundle();
            intent.putExtras(reqBundle);
        }
        startActivityForResult(intent, POST_TRANSLATION);
    }

    @Override
    public void queueTranslation(Request request) {

    }

    @Override
    public void deQueueTranslation(Request request) {

    }

    public void checkCompleteTranslation(Request request) {
        Intent intent = new Intent(this, TransCheckActivity.class);
        if(request != null) {
            Bundle reqBundle = request.toBundle();
            intent.putExtras(reqBundle);
        }
        startActivityForResult(intent, CHECK_COMPLETE_TRANSLATION);
    }
}
