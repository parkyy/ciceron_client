package starlord.ciceron.mvp.main;

import android.app.Activity;
import android.app.ListFragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import starlord.ciceron.mvp.common.Fonts;
import starlord.ciceron.mvp.collection.Request;

/**
 * A fragment representing a list of Items.
 * <p />
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class BaseListFragment extends ListFragment {

    protected OnInteractionListener mListener;
    private static Typeface font;

    public BaseListFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        font = Fonts.create(getActivity()).gotham();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        View view = getView().findViewById(android.R.id.content);
        View view = getActivity().getWindow().getDecorView();
        setGlobalFont(view);
    }

    @Override
    public void onResume() { super.onResume(); }

    @Override
    public void onPause()  { super.onPause();  }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setGlobalFont(View view) {
        if (view != null) {
            if(view instanceof ViewGroup){
                ViewGroup vg = (ViewGroup) view;
                int vgCnt = vg.getChildCount();
                for(int i=0; i < vgCnt; i++){
                    View v = vg.getChildAt(i);
                    if(v instanceof TextView){
                        ((TextView) v).setTypeface(font);
                    }
                    setGlobalFont(v);
                }
            }
        }
    }


    //abstract protected void initView(View view);
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnInteractionListener {
        public void initRequest();
        public void reviewRequest(Request request);
        public void checkCompleteRequest(Request request);
        public void cancelRequest(Request request);

        public void initTranslation(Request request);
        public void queueTranslation(Request request);
        public void deQueueTranslation(Request request);
        public void checkCompleteTranslation(Request request);
    }
}
